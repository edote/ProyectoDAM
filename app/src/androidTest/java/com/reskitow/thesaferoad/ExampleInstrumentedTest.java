package com.reskitow.thesaferoad;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.db.Manager;
import com.reskitow.thesaferoad.db.RelationContactManager;
import com.reskitow.thesaferoad.db.RelationsRouteManager;
import com.reskitow.thesaferoad.db.UserManager;
import com.reskitow.thesaferoad.model.Contact;
import com.reskitow.thesaferoad.model.Information;
import com.reskitow.thesaferoad.model.Method;
import com.reskitow.thesaferoad.model.Point;
import com.reskitow.thesaferoad.model.Route;
import com.reskitow.thesaferoad.model.Time;
import com.reskitow.thesaferoad.model.User;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    public Context useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        //assertEquals("com.reskitow.thesaferoad", appContext.getPackageName());
        return appContext;
    }

    @Test
    public void startBD() throws Exception {
        FactoryDB.init(useAppContext());
    }

    //Usuario se inserta OK.
    @Test
    public void insertarUsuarioTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        UserManager um = FactoryDB.getUserManager();
        assertTrue(um.add(u, false));
    }

    //Usuario no se inserta. (Es lo que tiene que hacer).
    @Test
    public void insertarUsuarioRepetidoTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        UserManager um = FactoryDB.getUserManager();
        assertFalse(um.add(u, false));
    }

    //Recupera valores del usuario bien.
    @Test
    public void recuperarUsuarioTest() throws Exception {
        startBD();
        //User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        UserManager um = FactoryDB.getUserManager();
        assertTrue(um.get("41625810L") == null);
        assertTrue(um.get("maatiascerezo@gmail.com").getLastName().equals("Cerezo"));
        assertTrue(um.get("maatiascerezo@gmail.com").getFirstName().equals("Matias"));
    }

    //No recupera valores de un usuario que no existe (Es lo que tiene que hacer).
    @Test
    public void recuperarUsuarioNoExistenteTest() throws Exception {
        startBD();
        UserManager um = FactoryDB.getUserManager();
        assertTrue(um.get("maatiascerezo2@gmail.com") == null);
    }

    //Usuario se actualiza correctamente.
    @Test
    public void actualizarUsuarioTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias2", "Cerezo2", new Date());
        UserManager um = FactoryDB.getUserManager();
        assertTrue(um.update(u, false));
    }

    //Usuario no se actualiza correctamente. (Es lo que tiene que hacer).
    @Test
    public void actualizarUsuarioNoExistenteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezodffedd@gmail.com", "Matias2", "Cerezo2", new Date());
        UserManager um = FactoryDB.getUserManager();
        assertFalse(um.update(u, false));
    }

    //Usuario se elimina correctamente.
    @Test
    public void deleteUserTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias2", "Cerezo2", new Date());
        UserManager um = FactoryDB.getUserManager();
        assertTrue(um.delete(u));
    }

    //Usuario no se elimina correctamente. Es lo que tiene que hacer).
    @Test
    public void deleteUserNoExistTest() throws Exception {
        startBD();
        User u = new User("maatiascerezodfsdsfsdf@gmail.com", "Matias2", "Cerezo2", new Date());
        UserManager um = FactoryDB.getUserManager();
        assertFalse(um.delete(u));
    }

    public List<Method> getListMethods() throws Exception {
        startBD();
        List<Method> methodList = new ArrayList<>();

        Method m = new Method("Method0", Method.Type.PHONE, "jajaMod");
        Method m1 = new Method("Method1", Method.Type.EMAIL, "jejeMod");
        Method m2 = new Method("Method2", Method.Type.PHONE, "jijiMod");

        /*Method m = new Method("Method0", Method.Type.EMAIL, "jaja");
        Method m1 = new Method("Method1", Method.Type.PHONE, "jeje");
        Method m2 = new Method("Method2", Method.Type.EMAIL, "jiji");*/

        methodList.add(m);
        methodList.add(m1);
        methodList.add(m2);

        return methodList;
    }

    //Devuelve una lista de contactos para pruebas.
    public List<Contact> getListContacts() throws Exception {
        startBD();
        List<Contact> contactList = new ArrayList<>();

        /*Contact c = new Contact("Contact2", "Contact2", "LastName", getListMethods());
        Contact c1 = new Contact("Contact3", "Contact3", "LastName", getListMethods());*/

        Contact c = new Contact("Contact2", "Contact2Mod", "LastNameMod", getListMethods());
        Contact c1 = new Contact("Contact3", "Contact3Mod", "LastNameMod", getListMethods());

        contactList.add(c);
        contactList.add(c1);
        return contactList;
    }

    //Añade methods OK.
    @Test
    public void addMethodTest() throws Exception {
        startBD();
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        Method m = new Method("Method0", Method.Type.EMAIL, "jojo");
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertTrue(rcm.addMethod(m, c.getId(), false));
    }

    //No añade methods OK. (Es lo que tiene que hacer).
    @Test
    public void addExistMethodTest() throws Exception {
        startBD();
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        Method m = new Method("MethodPrueba", Method.Type.EMAIL, "jojo");
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertFalse(rcm.addMethod(m, c.getId(), false));
    }

    //Añade lista methods OK.
    @Test
    public void addListMethodsTest() throws Exception {
        startBD();
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertTrue(rcm.addMethods(getListMethods(), c.getId()));
    }

    //Recupera OK.
    @Test
    public void getMethodTest() throws Exception {
        startBD();
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        Method m = new Method("Method0", Method.Type.EMAIL, "jojo");
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertTrue(rcm.getMethod(m.getId(), c.getId()) != null);
    }

    //Recupera list methods OK.
    @Test
    public void getListMethodsTest() throws Exception {
        startBD();
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        Method m = new Method("Method0", Method.Type.EMAIL, "jojo");
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertTrue(rcm.getMethods(c.getId()) != null);
    }

    //Actualiza methods OK.
    @Test
    public void updateMethodTest() throws Exception {
        startBD();
        Method m = new Method("Method0", Method.Type.EMAIL, "jojoMod");
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertTrue(rcm.updateMethod(m, c.getId(), false));
    }

    //Actualiza lista de methods OK.
    @Test
    public void updateMethodsTest() throws Exception {
        startBD();
        //Method m = new Method("Method1", Method.Type.PHONE, "jojoMod");
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertTrue(rcm.updateMethods(getListMethods(), c.getId()));
    }

    //Elimina lista de methods correctamente.
    @Test
    public void deleteListMethodsTest() throws Exception {
        startBD();
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertTrue(rcm.deleteMethods(c.getId()));
    }

    //No elimina lista de methods correctamente. (Tiene que hacer eso).
    @Test
    public void deleteNoExistListMethodsTest() throws Exception {
        startBD();
        Contact c = new Contact("Contact3", "Contacto1", "LastNameContact", getListMethods());
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertFalse(rcm.deleteMethods(c.getId()));
    }

    //Elimina lista de methods OK.
    @Test
    public void deleteMethodTest() throws Exception {
        startBD();
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        Method m = new Method("Method2", Method.Type.EMAIL, "jojo");
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertTrue(rcm.deleteMethod(m.getId(), c.getId()));
    }

    //No elimina lista de methods OK. (Tiene que hacer eso).
    @Test
    public void deleteNoExistMethodTest() throws Exception {
        startBD();
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        Method m = new Method("Methodfgd", Method.Type.EMAIL, "jojo");
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertFalse(rcm.deleteMethod(m.getId(), c.getId()));
    }

    //Obtiene synchronized de methods OK.
    @Test
    public void updateSynchronizedMethodTest() throws Exception {
        startBD();
        Method m = new Method("MethodPrueba", Method.Type.EMAIL, "jojo");
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertTrue(rcm.isMethodSynchronized(m.getId(), c.getId()));
    }

    //Actualiza de false a true de methods OK.
    @Test
    public void updateValorSyncMethodTest() throws Exception {
        startBD();
        Method m = new Method("MethodPrueba", Method.Type.EMAIL, "jojo");
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        assertTrue(rcm.setMethodSynchronized(m.getId(), c.getId()));
    }

    //Añadir contacto correctamente.
    @Test
    public void addContactTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertTrue(mc.add(c, u.getEmail(), false));
    }

    //Añadir lista de contactos correctamente.
    @Test
    public void addListContactTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertTrue(mc.add(getListContacts(), u.getEmail()));
    }

    //No añadir contacto repetido correctamente. (Es lo que tiene que hacer).
    @Test
    public void addContactExistTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contact1", "Contacto1", "LastNameContact", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertFalse(mc.add(c, u.getEmail(), false));
    }

    //Eliminar contacto OK.
    @Test
    public void deleteContactTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contact2", "Contacto4", "LastNameContact", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertTrue(mc.delete(c.getId(), u.getEmail()));
    }

    //No eliminar contacto OK. (Es asi).
    @Test
    public void deleteContactNoExistTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contactgfdgd", "Contacto4", "LastNameContact", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertFalse(mc.delete(c.getId(), u.getEmail()));
    }

    //Eliminar lista de contactos OK.
    @Test
    public void deleteAllContactsTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertTrue(mc.deleteAll(u.getEmail()));
    }

    //No eliminar lista de contactos OK. (Es asi).
    @Test
    public void deleteAllContactsNoExistTest() throws Exception {
        startBD();
        User u = new User("maatiascerezdfgdo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertFalse(mc.deleteAll(u.getEmail()));
    }

    //Recupera contacto OK.
    @Test
    public void getContactTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contact2", "Contacto1", "LastNameContact", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertTrue(mc.get(c.getId(), u.getEmail()).getFirstName().equals("Contact2Mod"));
    }

    //No recupera contacto OK (Es lo que tiene que hacer).
    @Test
    public void getContactNoExistTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contactgfdgd", "Contacto4", "LastNameContact", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertFalse(mc.delete(c.getId(), u.getEmail()));
    }

    //Recupera lista de contactos OK.
    @Test
    public void getListContactTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertFalse(mc.getAll(u.getEmail()).isEmpty());
    }

    //No recupera contacto OK (Es lo que tiene que hacer).
    @Test
    public void getListContactNoExistTest() throws Exception {
        startBD();
        User u = new User("maatiascerezodfgd@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertTrue(mc.getAll(u.getEmail()).isEmpty());
    }

    //Actualiza contacto OK.
    @Test
    public void updateContactTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contact2", "ContactoMod", "mod", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertTrue(mc.update(c, u.getEmail(), false));
    }

    //No actualiza contacto OK. (Es lo que tiene que hacer).
    @Test
    public void updateContactNoExistTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contactgfdgd", "Contacto4", "LastNameContact", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertFalse(mc.update(c, u.getEmail(), false));
    }

    //Actualiza lista de contacto OK.
    @Test
    public void updateListContactTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertTrue(mc.update(getListContacts(), u.getEmail()));
    }

    //No actualiza la lista de contacto OK. (Es lo que tiene que hacer).
    @Test
    public void updateNoExistListContactTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.comdfs", "Matias", "Cerezo", new Date());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertFalse(mc.update(getListContacts(), u.getEmail()));
    }

    //Obtiene los contactos no sincronizados OK. (Igual a 0)
    @Test
    public void getAllNotSyncContactTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertFalse(mc.getAllWhereIsNotSynchronized(u.getEmail()).isEmpty());
    }

    //Comprueba si el contacto está sincronizado o no OK.
    @Test
    public void getSyncContactTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contact3", "dsfsd", "dsfs", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertTrue(mc.isSynchronized(c.getId(), u.getEmail()));
    }

    //Np comprueba si el contacto está sincronizado o no OK. (Tiene que hacer eso).
    @Test
    public void getSyncContactNoExistTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contactefgegerg", "dsfsd", "dsfs", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertFalse(mc.isSynchronized(c.getId(), u.getEmail()));
    }

    //Cambia el valor de sincronizado OK.
    @Test
    public void setSyncContactTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contact1", "dsfsd", "dsfs", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertTrue(mc.setSynchronized(c.getId(), u.getEmail()));
    }

    //No cambia el valor de sincronizado OK. (Tiene que hacer eso).
    @Test
    public void setSyncContactNoExistTest() throws Exception {
        startBD();
        User u = new User("maatiascsdferezo@gmail.com", "Matias", "Cerezo", new Date());
        Contact c = new Contact("Contactedffdssdf1", "dsfsd", "dsfs", getListMethods());
        Manager<Contact> mc = FactoryDB.getContactManager();
        assertFalse(mc.setSynchronized(c.getId(), u.getEmail()));
    }

    //Obtener una lista de rutas.
    public List<Route> getRoutesList() {
        List<Route> routeList = new ArrayList<>();

        //Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        Route r1 = new Route("Ruta3", "Ruta3", new Time(new Date(), new Date()));
        Route r2 = new Route("Ruta4", "Ruta4", new Time(new Date(), new Date()));

        //routeList.add(r);
        routeList.add(r1);
        routeList.add(r2);

        return routeList;
    }

    //Añadir ruta OK. --------------> Añade rutas con una foreign que no existe.
    @Test
    public void addRouteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertTrue(mc.add(r, u.getEmail(), false));
    }

    //Añadir lista de rutas OK.
    @Test
    public void addListRouteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertTrue(mc.add(getRoutesList(), u.getEmail()));
    }

    //No añade ruta repetida OK. (Es lo que tiene que hacer).
    @Test
    public void addExistRouteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Route r = new Route("Ruta1", "Ruta1", new Time(new Date(), new Date()));
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertFalse(mc.add(r, u.getEmail(), false));
    }

    //Actualiza OK.
    @Test
    public void updateRouteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Route r = new Route("Ruta1", "Ruta1Mod", new Time(new Date(), new Date()));
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertTrue(mc.update(r, u.getEmail(), false));
    }

    //No actualiza OK. (Es lo que tiene que hacer).
    @Test
    public void updateNoExistRouteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Route r = new Route("Rutdsfsda1", "Ruta1dfsdfMod", new Time(new Date(), new Date()));
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertFalse(mc.update(r, u.getEmail(), false));
    }

    //Elimina rutas OK.
    @Test
    public void deleteRouteTest() throws Exception {
        startBD();
        Manager<Route> mr = FactoryDB.getRouteManager();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        assertTrue(mr.delete(r.getId(), u.getEmail()));
    }

    //Elimina todas las rutas OK.
    @Test
    public void deleteAllRoutesTest() throws Exception {
        startBD();
        Manager<Route> mr = FactoryDB.getRouteManager();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        assertTrue(mr.deleteAll(u.getEmail()));
    }

    //Elimina rutas OK.
    @Test
    public void deleteNoExistRouteTest() throws Exception {
        startBD();
        Manager<Route> mr = FactoryDB.getRouteManager();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Route r = new Route("Ruta1", "Ruta1", new Time(new Date(), new Date()));
        assertFalse(mr.delete(r.getId(), u.getEmail()));
    }

    //Recupera Ok.
    @Test
    public void getRouteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Route r = new Route("Ruta1", "Ruta1Mod", new Time(new Date(), new Date()));
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertTrue(mc.get(r.getId(), u.getEmail()).getName().equals("Ruta1Mod"));
    }

    //No recupera OK. (Es lo que tiene que hacer).
    @Test
    public void getNoExistRouteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Route r = new Route("Rutadsfs1", "Ruta1Mod", new Time(new Date(), new Date()));
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertTrue(mc.get(r.getId(), u.getEmail()) == null);
    }

    //Recupera lista OK.
    @Test
    public void getListRouteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertFalse(mc.getAll(u.getEmail()).isEmpty());
    }

    //No recupera OK. (Es lo que tiene que hacer).
    @Test
    public void getListRouteNoExistTest() throws Exception {
        startBD();
        User u = new User("maatiascerezsadsado@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertFalse(mc.getAll(u.getEmail()) == null);
    }

    //Recupera lista OK. (En caso de no tener sincronizados, va OK).
    @Test
    public void getAllNoSyncRouteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertTrue(mc.getAllWhereIsNotSynchronized(u.getEmail()).isEmpty());
    }

    //Funciona OK. (Corregir consulta).
    @Test
    public void getSyncRouteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Route r = new Route("Ruta1", "Ruta1Mod", new Time(new Date(), new Date()));
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertTrue(mc.isSynchronized(r.getId(), u.getEmail()));
    }

    //Funciona OK. (Corregir consulta).
    @Test
    public void setSyncRouteTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Route r = new Route("Ruta2", "Ruta1Mod", new Time(new Date(), new Date()));
        Manager<Route> mc = FactoryDB.getRouteManager();
        assertTrue(mc.setSynchronized(r.getId(), u.getEmail()));
    }

    private List<Point> getListPoints() {
        List<Point> pointList = new ArrayList<>();

        Point p = new Point(4.55, 8.11, new Date());
        Point p1 = new Point(8.55, 18.11, new Date());
        Point p2 = new Point(9.55, 18.11, new Date());

        pointList.add(p);
        pointList.add(p1);
        pointList.add(p2);
        return pointList;
    }

    //Añadir punto OK.
    @Test
    public void addPointTest() throws Exception {
        startBD();
        Point p = new Point(4.55, 8.11, new Date());
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        assertTrue(rrm.addPoint(p, r.getId(), false));
    }

    //Añadir lista de puntos OK.
    @Test
    public void addPointListTest() throws Exception {
        startBD();
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        assertTrue(rrm.addPoints(getListPoints(), r.getId()));
    }

    //Eliminar puntos de una ruta OK.
    @Test
    public void deletePointsTest() throws Exception {
        startBD();
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        assertTrue(rrm.deletePoints(r.getId()));
    }

    //Recupera lista de puntos OK.
    @Test
    public void getPointsTest() throws Exception {
        startBD();
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        assertFalse(rrm.getPoints(r.getId()).isEmpty());
    }

    //Recupera lista de puntos no sync OK.
    @Test
    public void getPointsNotSyncTest() throws Exception {
        startBD();
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        assertTrue(rrm.getPointsNotSynchronized(r.getId()).isEmpty());
    }

    //Recupera lista de puntos OK.
    @Test
    public void setPointsSyncTest() throws Exception {
        startBD();
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        assertTrue(rrm.setPointsSynchronized(r.getId()));
    }

    //Añade time OK.
    @Test
    public void addTimeTest() throws Exception {
        startBD();
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        Time t = new Time(new Date(), new Date());
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        assertTrue(rrm.addTime(t, r.getId(), false));
    }

    //Elimina time OK.
    @Test
    public void deleteTimeTest() throws Exception {
        startBD();
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        Route r = new Route("Ruta4", "Ruta2", new Time(new Date(), new Date()));
        assertTrue(rrm.deleteTime(r.getId()));
    }

    //No elimina time OK. (Es asi).
    @Test
    public void deleteTimeNoExistTest() throws Exception {
        startBD();
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        Route r = new Route("Rutadsfsd", "Ruta2", new Time(new Date(), new Date()));
        assertFalse(rrm.deleteTime(r.getId()));
    }

    //Actualizar Time OK.
    @Test
    public void updateTimeTest() throws Exception {
        startBD();
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        assertTrue(rrm.updateTime(new Time(new Date(), new Date()), r.getId(), false));
    }

    //Get Time OK.
    @Test
    public void getTimeTest() throws Exception {
        startBD();
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        assertTrue(rrm.get(r.getId()).getStartDate() != null);
    }

    //get Sync Time OK.
    @Test
    public void getIsTimeSyncTest() throws Exception {
        startBD();
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        assertTrue(rrm.isTimeSynchronized(r.getId()));
    }

    //Set Sync Time OK.
    @Test
    public void setIsTimeSyncTest() throws Exception {
        startBD();
        Route r = new Route("Ruta2", "Ruta2", new Time(new Date(), new Date()));
        RelationsRouteManager rrm = FactoryDB.getRelationsRouteManager();
        assertTrue(rrm.setTimeSynchronized(r.getId()));
    }


    private List<Information> getListInfo() {
        List<Information> informationList = new ArrayList<>();
        Information i = new Information("Info 2", "TypeInfo2", "fbdhjsdjifsanhubdf", 1);
        Information i1 = new Information("Info 3", "TypeInfo3", "fbdhjsdjifsanhubdf", 0);

        informationList.add(i);
        informationList.add(i1);

        return informationList;
    }

    //Añadir info OK.
    @Test
    public void addInformationTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Information i = new Information("Info 2", "TypeInfo1", "fbdhjsdjifsanhubdf", 200);
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertTrue(mi.add(i, u.getEmail(), false));
    }

    //No añadir info OK.
    @Test
    public void addExistInformationTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Information i = new Information("Info 2", "TypeInfo1", "fbdhjsdjifsanhubdf", 200);
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertFalse(mi.add(i, u.getEmail(), false));
    }

    //Añadir lista de info OK.
    @Test
    public void addListInformationTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertTrue(mi.add(getListInfo(), u.getEmail()));
    }

    //Update info OK.
    @Test
    public void updateInformationTest() throws Exception {
        startBD();
        Information i = new Information("Info 1", "ModType", "fbdhjsdjifsanhubdf", 200);
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertTrue(mi.update(i, u.getEmail(), false));
    }

    //No update info OK.
    @Test
    public void updateNoExistInformationTest() throws Exception {
        startBD();
        Information i = new Information("Info dsff1", "ModType", "fbdhjsdjifsanhubdf", 200);
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertFalse(mi.update(i, u.getEmail(), false));
    }

    //OK
    @Test
    public void deleteInformationTest() throws Exception {
        startBD();
        Information i = new Information("Info 1ss", "ModType", "fbdhjsdjifsanhubdf", 200);
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertTrue(mi.delete(i.getId(), u.getEmail()));
    }

    //Elimina OK.
    @Test
    public void deleteAllInformationTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertTrue(mi.deleteAll(u.getEmail()));
    }

    //Recupera Info OK.
    @Test
    public void getInformationTest() throws Exception {
        startBD();
        Information i = new Information("Info 1", "ModType", "fbdhjsdjifsanhubdf", 200);
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertTrue(mi.get(i.getId(), u.getEmail()).getSeriousness() == 200);
    }

    //No recupera Info OK. (Es asi).
    @Test
    public void getNoExistInformationTest() throws Exception {
        startBD();
        Information i = new Information("Info dfsdf1", "ModType", "fbdhjsdjifsanhubdf", 200);
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertTrue(mi.get(i.getId(), u.getEmail()) == null);
    }

    //Lista OK.
    @Test
    public void getAllInformationTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertFalse(mi.getAll(u.getEmail()).isEmpty());
    }

    //Va OK.
    @Test
    public void isSyncInformationTest() throws Exception {
        startBD();
        Information i = new Information("Info 1", "ModType", "fbdhjsdjifsanhubdf", 200);
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertTrue(mi.isSynchronized(i.getId(), u.getEmail()));
    }

    //ESTO VA OK.
    @Test
    public void setSyncInformationTest() throws Exception {
        startBD();
        Information i = new Information("Info 1", "ModType", "fbdhjsdjifsanhubdf", 200);
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertTrue(mi.setSynchronized(i.getId(), u.getEmail()));
    }

    //Perfect. (Select mal). Corregido.
    @Test
    public void getAllNoSyncInformationTest() throws Exception {
        startBD();
        User u = new User("maatiascerezo@gmail.com", "Matias", "Cerezo", new Date());
        Manager<Information> mi = FactoryDB.getInformationManager();
        assertFalse(mi.getAllWhereIsNotSynchronized(u.getEmail()).isEmpty());
    }
}