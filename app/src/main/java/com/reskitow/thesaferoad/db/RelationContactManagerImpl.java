package com.reskitow.thesaferoad.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.reskitow.thesaferoad.model.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que realiza la implementación de RelationContactManager.
 */
class RelationContactManagerImpl implements RelationContactManager {

    protected DataBaseHelper mDataBaseHelper;

    RelationContactManagerImpl(final DataBaseHelper dataBaseHelper) {
        this.mDataBaseHelper = dataBaseHelper;
    }

    @Override
    public boolean addMethod(Method method, Object contactId, boolean isSynchronized) {
        long rowId = mDataBaseHelper.getWritableDatabase().insert(InfoDB.TABLE_METHODS, null,
                getMethodContentValues(method, contactId.toString(), isSynchronized, true));
        return rowId != -1;
    }

    @Override
    public boolean addMethods(List<Method> methods, Object contactId) {
        int methodsAdded = 0;
        for (Method method : methods) {
            if (addMethod(method, contactId, true)) methodsAdded++;
        }
        return methodsAdded == methods.size();
    }

    @Override
    public boolean updateMethod(Method method, Object contactId, boolean isSynchronized) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_METHODS,
                getMethodContentValues(method, null, isSynchronized, false),
                getWhereClause(false), new String[]{method.getId(), contactId.toString()});
        return rowsAffected > 0;
    }

    @Override
    public boolean updateMethods(List<Method> methods, Object contactId) {
        int methodsUpdated = 0;
        for (Method method : methods) {
            if (updateMethod(method, contactId, true)) methodsUpdated++;
        }
        return methodsUpdated == methods.size();
    }

    @Override
    public boolean deleteMethod(Object... primaryKeys) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().delete(InfoDB.TABLE_METHODS,
                getWhereClause(false), new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        return rowsAffected > 0;
    }

    @Override
    public boolean deleteMethods(Object contactId) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().delete(InfoDB.TABLE_METHODS,
                getWhereClause(true), new String[]{contactId.toString()});
        return rowsAffected > 0;
    }

    @Override
    public Method getMethod(Object... primaryKeys) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + getMethodColumnsSelect()
                        + " FROM " + InfoDB.TABLE_METHODS + " WHERE " + getWhereClause(false),
                new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        Method method = null;
        if (cursor.moveToFirst()) {
            method = getMethodFromCursor(cursor);
        }
        cursor.close();
        return method;
    }

    @Override
    public List<Method> getMethods(Object contactId) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + getMethodColumnsSelect()
                + " FROM " + InfoDB.TABLE_METHODS + " WHERE "
                + getWhereClause(true), new String[]{contactId.toString()});
        List<Method> methods = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                methods.add(getMethodFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return methods;
    }

    @Override
    public boolean isMethodSynchronized(Object... primaryKeys) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + InfoDB.KEY_IS_SYNCHRONIZED + " FROM " + InfoDB.TABLE_METHODS +
                " WHERE " + InfoDB.METHOD_KEY_ID + " = ? AND " + InfoDB.FK_CONTACT_ID + " = ? ", new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        boolean isSync = false;
        if (cursor.moveToFirst()) {
            isSync = cursor.getInt(0) != 0;
        }
        cursor.close();
        return isSync;
    }

    @Override
    public boolean setMethodSynchronized(Object... primaryKeys) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_METHODS,
                getIsSynchronizedContentValues(), InfoDB.METHOD_KEY_ID + " = ? AND " +
                        InfoDB.FK_CONTACT_ID + " = ?",
                new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        return rowsAffected > 0;
    }

    private ContentValues getMethodContentValues(Method method, String contactId, boolean isSynchronized, boolean putPrimaryKeys) {
        ContentValues cv = new ContentValues(putPrimaryKeys ? 5 : 3);
        if (putPrimaryKeys) {
            cv.put(InfoDB.METHOD_KEY_ID, method.getId());
            cv.put(InfoDB.FK_CONTACT_ID, contactId);
        }
        cv.put(InfoDB.METHOD_KEY_TYPE, method.getType().ordinal());
        cv.put(InfoDB.METHOD_KEY_FIELD, method.getField());
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, isSynchronized);
        return cv;
    }

    private ContentValues getIsSynchronizedContentValues() {
        ContentValues cv = new ContentValues(1);
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, true);
        return cv;
    }

    private String getMethodColumnsSelect() {
        return InfoDB.METHOD_KEY_ID + ", " + InfoDB.METHOD_KEY_TYPE + ", " + InfoDB.METHOD_KEY_FIELD;
    }

    private Method getMethodFromCursor(Cursor cursor) {
        return new Method(cursor.getString(0), cursor.getInt(1) == 0 ? Method.Type.EMAIL :
                Method.Type.PHONE, cursor.getString(2));
    }

    private String getWhereClause(boolean onlyFK) {
        return (onlyFK ? "" : InfoDB.METHOD_KEY_ID + " = ? AND ") + InfoDB.FK_CONTACT_ID + " = ?";
    }
}
