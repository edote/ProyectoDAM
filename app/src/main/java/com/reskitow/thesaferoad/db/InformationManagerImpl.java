package com.reskitow.thesaferoad.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.reskitow.thesaferoad.model.Information;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que realiza la implementación de Manager con objeto Information.
 */
class InformationManagerImpl implements Manager<Information> {

    private DataBaseHelper mDataBaseHelper;

    InformationManagerImpl(final DataBaseHelper dataBaseHelper) {
        this.mDataBaseHelper = dataBaseHelper;
    }

    @Override
    public boolean add(Information information, Object foreignKey, boolean isSynchronized) {
        long rowId = mDataBaseHelper.getWritableDatabase().insert(InfoDB.TABLE_INFORMATIONS, null,
                getInformationContentValues(information, foreignKey.toString(), isSynchronized, true));
        return rowId != -1;
    }

    @Override
    public boolean add(List<Information> informations, Object foreignKey) {
        int infoAdded = 0;
        for (Information info : informations) {
            if (add(info, foreignKey, true)) infoAdded++;
        }
        return infoAdded == informations.size();
    }

    @Override
    public boolean update(Information information, Object foreignKey, boolean isSynchronized) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_INFORMATIONS, getInformationContentValues(information,
                null, isSynchronized, false), InfoDB.INFORMATION_KEY_ID + " = ? AND " + InfoDB.FK_USER_ID + " = ?",
                new String[]{information.getId(), foreignKey.toString()});
        return rowsAffected > 0;
    }

    @Override
    public boolean update(List<Information> informations, Object foreignKey) {
        int infosUpdated = 0;
        for (Information info : informations) {
            if (update(info, foreignKey, true)) infosUpdated++;
        }
        return infosUpdated == informations.size();
    }

    @Override
    public boolean delete(Object... primaryKeys) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().delete(InfoDB.TABLE_INFORMATIONS, InfoDB.INFORMATION_KEY_ID +
                " = ? AND " + InfoDB.FK_USER_ID + " = ? ", new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        return rowsAffected > 0;
    }

    @Override
    public boolean deleteAll(Object foreignKey) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().delete(InfoDB.TABLE_INFORMATIONS,
                InfoDB.FK_USER_ID + " = ?", new String[]{foreignKey.toString()});
        return rowsAffected > 0;
    }

    @Override
    public Information get(Object... primaryKeys) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + getInformationColumnsSelect() +
                " FROM " + InfoDB.TABLE_INFORMATIONS + " WHERE " + InfoDB.INFORMATION_KEY_ID + " = ? AND " +
                InfoDB.FK_USER_ID + " = ?", new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        Information info = null;
        if (cursor.moveToFirst()) {
            info = getInformationFromCursor(cursor);
        }
        cursor.close();
        return info;
    }

    @Override
    public List<Information> getAll(Object foreignKey) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + getInformationColumnsSelect()
                        + " FROM " + InfoDB.TABLE_INFORMATIONS + " WHERE " + InfoDB.FK_USER_ID + " = ?",
                new String[]{foreignKey.toString()});
        return getListInformations(cursor);
    }

    private List<Information> getListInformations(Cursor cursor) {
        List<Information> listInformations = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                listInformations.add(getInformationFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return listInformations;
    }

    @Override
    public boolean isSynchronized(Object... primaryKeys) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + InfoDB.KEY_IS_SYNCHRONIZED + " FROM " +
                        InfoDB.TABLE_INFORMATIONS + " WHERE " + InfoDB.INFORMATION_KEY_ID + " = ? AND " + InfoDB.FK_USER_ID + " = ?",
                new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        boolean isSync = false;
        if (cursor.moveToFirst()) {
            isSync = cursor.getInt(0) != 0;
        }
        cursor.close();
        return isSync;
    }

    @Override
    public boolean setSynchronized(Object... primaryKeys) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_INFORMATIONS, getIsSynchronizedContentValues(),
                InfoDB.INFORMATION_KEY_ID + " = ? AND " + InfoDB.FK_USER_ID + " = ?", new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        return rowsAffected > 0;
    }

    @Override
    public List<Information> getAllWhereIsNotSynchronized(Object foreignKey) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + getInformationColumnsSelect() + " FROM " + InfoDB.TABLE_INFORMATIONS +
                " WHERE " + InfoDB.KEY_IS_SYNCHRONIZED + " = 0 AND " + InfoDB.FK_USER_ID + " = ?", new String[]{foreignKey.toString()});
        return getListInformations(cursor);
    }

    private ContentValues getInformationContentValues(Information info, String userId, boolean isSynchronized, boolean putPrimaryKeys) {
        ContentValues cv = new ContentValues(putPrimaryKeys ? 6 : 4);
        if (putPrimaryKeys) {
            cv.put(InfoDB.INFORMATION_KEY_ID, info.getId());
            cv.put(InfoDB.FK_USER_ID, userId);
        }
        cv.put(InfoDB.INFORMATION_KEY_TYPE, info.getType());
        cv.put(InfoDB.INFORMATION_KEY_DESCRIPTION, info.getDescription());
        cv.put(InfoDB.INFORMATION_KEY_SERIOUSNESS, info.getSeriousness());
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, isSynchronized);
        return cv;
    }

    private String getInformationColumnsSelect() {
        return InfoDB.INFORMATION_KEY_ID + " , " + InfoDB.INFORMATION_KEY_TYPE + " , " + InfoDB.INFORMATION_KEY_DESCRIPTION + " , " + InfoDB.INFORMATION_KEY_SERIOUSNESS;
    }

    private Information getInformationFromCursor(Cursor cursor) {
        return new Information(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3));
    }

    private ContentValues getIsSynchronizedContentValues() {
        ContentValues cv = new ContentValues(1);
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, true);
        return cv;
    }

}
