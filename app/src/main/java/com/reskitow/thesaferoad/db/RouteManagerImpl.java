package com.reskitow.thesaferoad.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.reskitow.thesaferoad.model.Point;
import com.reskitow.thesaferoad.model.Route;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que realiza la implementación de Manager con objeto Route.
 */
class RouteManagerImpl extends RelationsRouteManagerImpl implements Manager<Route> {

    RouteManagerImpl(final DataBaseHelper dataBaseHelper) {
        super(dataBaseHelper);
    }

    @Override
    public boolean add(Route route, Object foreignKey, boolean isSynchronized) {
        long rowId = mDataBaseHelper.getWritableDatabase().insert(InfoDB.TABLE_ROUTES, null,
                getRouteContentValues(route, foreignKey, isSynchronized, true));
        if (rowId != -1) {
            addTime(route.getTime(), route.getId(), isSynchronized);
            if (route.getPoints() != null) {
                for (Point point : route.getPoints()) {
                    addPoint(point, route.getId(), isSynchronized);
                }
            }
        }
        return rowId != -1;
    }

    @Override
    public boolean add(List<Route> routes, Object foreignKey) {
        int routesAdded = 0;
        for (Route route : routes) {
            if (add(route, foreignKey, true)) routesAdded++;
        }
        return routesAdded == routes.size();
    }

    @Override
    public boolean update(Route route, Object foreignKey, boolean isSynchronized) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_ROUTES,
                getRouteContentValues(route, null, isSynchronized, false), InfoDB.ROUTE_KEY_ID + " = ? AND "
                        + InfoDB.FK_USER_ID + " = ?", new String[]{route.getId(), foreignKey.toString()});
        if(rowsAffected > 0){
            updateTime(route.getTime(), route.getId(), isSynchronized);
        }
        return rowsAffected > 0;
    }

    @Override
    public boolean update(List<Route> routes, Object foreignKey) {
        int routesUpdated = 0;
        for (Route route : routes) {
            if (update(route, foreignKey, true)) routesUpdated++;
        }
        return routesUpdated > 0;
    }

    @Override
    public boolean delete(Object... primaryKeys) {
        deletePoints(primaryKeys[0].toString());
        deleteTime(primaryKeys[0].toString());
        int rowsAffected = mDataBaseHelper.getWritableDatabase().delete(InfoDB.TABLE_ROUTES,
                InfoDB.ROUTE_KEY_ID + " = ? AND " + InfoDB.FK_USER_ID + " = ?",
                new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        return rowsAffected == 1;
    }

    @Override
    public boolean deleteAll(Object foreignKey) {
        for (Route route : getAll(foreignKey)) {
            deletePoints(route.getId());
            deleteTime(route.getId());
        }
        int rowsAffected = mDataBaseHelper.getWritableDatabase().delete(InfoDB.TABLE_ROUTES,
                InfoDB.FK_USER_ID + " = ?", new String[]{foreignKey.toString()});
        return rowsAffected > 0;
    }

    @Override
    public Route get(Object... primaryKeys) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + getRouteColumnsSelect() + " FROM " + InfoDB.TABLE_ROUTES
                        + " WHERE " + InfoDB.ROUTE_KEY_ID + " = ? AND " + InfoDB.FK_USER_ID + " = ? ",
                new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        Route route = null;
        if (cursor.moveToFirst()) {
            route = getRouteFromCursor(cursor);
            route.setTime(get(route.getId()));
            route.setPoints(getPoints(route.getId()));
        }
        cursor.close();
        return route;
    }

    @Override
    public List<Route> getAll(Object foreignKey) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + getRouteColumnsSelect()
                + " FROM " + InfoDB.TABLE_ROUTES + " WHERE " + InfoDB.FK_USER_ID + " = ?", new String[]{foreignKey.toString()});
        List<Route> listAllRoutes = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                listAllRoutes.add(getRouteFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return listAllRoutes;
    }


    @Override
    public boolean isSynchronized(Object... primaryKeys) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + InfoDB.KEY_IS_SYNCHRONIZED + " FROM " + InfoDB.TABLE_ROUTES
                + " WHERE " + InfoDB.ROUTE_KEY_ID + " = ? AND " + InfoDB.FK_USER_ID + " = ?", new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        boolean isSync = false;
        if (cursor.moveToFirst()) {
            isSync = cursor.getInt(0) != 0;
        }
        cursor.close();
        return isSync;
    }

    @Override
    public boolean setSynchronized(Object... primaryKeys) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_ROUTES,
                getIsSynchronizedContentValues(), InfoDB.ROUTE_KEY_ID + " = ? AND " +
                        InfoDB.FK_USER_ID + " = ?", new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        return rowsAffected > 0;
    }

    @Override
    public List<Route> getAllWhereIsNotSynchronized(Object foreignKey) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + InfoDB.KEY_IS_SYNCHRONIZED + " FROM " + InfoDB.TABLE_ROUTES +
                " WHERE " + InfoDB.KEY_IS_SYNCHRONIZED + " = 0 AND " + InfoDB.FK_USER_ID + " = ?", new String[]{foreignKey.toString()});
        List<Route> listRoutesNotSync = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                listRoutesNotSync.add(getRouteFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        return listRoutesNotSync;
    }

    private ContentValues getRouteContentValues(Route route, Object foreignKey, boolean isSynchronized,
                                                boolean putPrimaryKeys) {
        ContentValues cv = new ContentValues(putPrimaryKeys ? 4 : 2);
        if (putPrimaryKeys) {
            cv.put(InfoDB.ROUTE_KEY_ID, route.getId());
            cv.put(InfoDB.FK_USER_ID, foreignKey.toString());
        }
        cv.put(InfoDB.ROUTE_KEY_NAME, route.getName());
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, isSynchronized);
        return cv;
    }

    private Route getRouteFromCursor(Cursor cursor) {
        return new Route(cursor.getString(0), cursor.getString(1), get(cursor.getString(0)), getPoints(cursor.getString(0)));
    }

    private String getRouteColumnsSelect() {
        return InfoDB.ROUTE_KEY_ID + " , " + InfoDB.ROUTE_KEY_NAME;
    }

    private ContentValues getIsSynchronizedContentValues() {
        ContentValues cv = new ContentValues(1);
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, true);
        return cv;
    }

}
