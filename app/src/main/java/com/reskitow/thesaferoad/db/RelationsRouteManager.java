package com.reskitow.thesaferoad.db;

import com.reskitow.thesaferoad.model.Point;
import com.reskitow.thesaferoad.model.Time;

import java.util.List;

/**
 * Acciones que deben realizar la implementaciones de esta interfaz (Gestor de relaciones de Route).
 */
public interface RelationsRouteManager {

    /**
     * Añade un punto a la base de datos.
     *
     * @param point          Punto ha añadir.
     * @param routeId        Foreign key de la ruta con la que esta relacionada.
     * @param isSynchronized true si se ha sincronizado con la base de datos externa, false en caso contrario.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean addPoint(Point point, Object routeId, boolean isSynchronized);

    /**
     * Añade los puntos a la base de datos.
     *
     * @param point   Puntos ha añadir, isSynchronized será true si se añaden listas.
     * @param routeId Foreign key de la ruta con la que esta relacionada.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean addPoints(List<Point> point, Object routeId);

    /**
     * Elimina todos los puntos relacionados con una ruta.
     *
     * @param routeId Foreign key de la ruta con la que esta relacionada.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean deletePoints(Object routeId);

    /**
     * Obtiene todos los puntos relacionados con una ruta.
     *
     * @param routeId Foreign key de la ruta con la que esta relacionada.
     * @return Lista de puntos relacionados con la ruta.
     */
    List<Point> getPoints(Object routeId);

    /**
     * Cambia todos los puntos de la ruta a true en el campo isSynchronized.
     *
     * @param routeId Foreign key de la ruta con la que esta relacionada.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean setPointsSynchronized(Object routeId);

    /**
     * Recupera todos los puntos que no están sincronizados.
     *
     * @param routeId Foreign key de la ruta con la que esta relacionada.
     * @return Lista con los puntos no sincronizados.
     */
    List<Point> getPointsNotSynchronized(Object routeId);

    /**
     * Añade el tiempo (BD).
     *
     * @param time           Time ha añadir.
     * @param routeId        Foreign key de la ruta con la que esta relacionada.
     * @param isSynchronized true si se ha sincronizado con la base de datos externa, false en caso contrario.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean addTime(Time time, Object routeId, boolean isSynchronized);

    /**
     * Actualiza el tiempo de la base de datos.
     *
     * @param time           Time ha actualizar.
     * @param routeId        Foreign key de la ruta con la que esta relacionada.
     * @param isSynchronized true si se ha sincronizado con la base de datos externa, false en caso contrario.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean updateTime(Time time, Object routeId, boolean isSynchronized);

    /**
     * Elimina el tiempo relacionado con la ruta.
     *
     * @param primaryKey Foreign key de la ruta con la que esta relacionada.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean deleteTime(Object primaryKey);

    /**
     * Obtiene el tiempo relacionado con la ruta.
     *
     * @param routeId Foreign key de la ruta con la que esta relacionada.
     * @return Tiempo encontrado o null en caso contrario.
     */
    Time get(Object routeId);

    /**
     * Cambia a true el campo isSynchronized del tiempo relacionado con la ruta.
     *
     * @param routeId Foreign key de la ruta con la que esta relacionada.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean setTimeSynchronized(Object routeId);

    /**
     * Comprueba si está sincronizado con la base de datos externa.
     *
     * @param routeId Foreign key de la ruta con la que esta relacionada.
     * @return true si el tiempo está sincronizado con la base de datos externa o false en caso contrario.
     */
    boolean isTimeSynchronized(Object routeId);

}
