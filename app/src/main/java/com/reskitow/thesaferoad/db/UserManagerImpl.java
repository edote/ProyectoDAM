package com.reskitow.thesaferoad.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.reskitow.thesaferoad.model.User;
import com.reskitow.thesaferoad.utils.DateParser;

/**
 * Clase que implementa UserManager.
 */
class UserManagerImpl implements UserManager {

    private DataBaseHelper mDataBaseHelper;

    UserManagerImpl(DataBaseHelper dataBaseHelper) {
        this.mDataBaseHelper = dataBaseHelper;
    }

    @Override
    public boolean add(User user, boolean isSynchronized) {
        long insert = mDataBaseHelper.getWritableDatabase().insert(InfoDB.TABLE_USERS, null,
                getContentValues(user, true, isSynchronized));
        return insert != -1;
    }

    @Override
    public boolean update(User user, boolean isSynchronized) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_USERS,
                getContentValues(user, false, isSynchronized), InfoDB.USER_KEY_EMAIL
                        + " = ?", new String[]{user.getEmail()});
        return rowsAffected > 0;
    }

    @Override
    public boolean delete(User user) {
        return delete(user.getEmail());
    }

    @Override
    public boolean delete(String userId) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().delete(InfoDB.TABLE_USERS,
                InfoDB.USER_KEY_EMAIL + " = ?", new String[]{userId});
        return rowsAffected > 0;
    }

    @Override
    public User get(String userId) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT * FROM "
                        + InfoDB.TABLE_USERS + " WHERE " + InfoDB.USER_KEY_EMAIL + " = ?",
                new String[]{userId});
        User user = null;
        if (cursor.moveToFirst()) {
            user = getFromCursor(cursor);
        }
        return user;
    }

    private User getFromCursor(Cursor cursor) {
        return new User(cursor.getString(0), cursor.getString(1), cursor.getString(2),
                DateParser.parse(cursor.getString(3), DateParser.FORMAT_BIRTHDAY));
    }

    private ContentValues getContentValues(User user, boolean putPrimaryKey, boolean isSynchronized) {
        ContentValues cv = new ContentValues(putPrimaryKey ? 5 : 4);
        if (putPrimaryKey) cv.put(InfoDB.USER_KEY_EMAIL, user.getEmail());
        cv.put(InfoDB.USER_KEY_FIRST_NAME, user.getFirstName());
        cv.put(InfoDB.USER_KEY_LAST_NAME, user.getLastName());
        cv.put(InfoDB.USER_KEY_BIRTHDAY, DateParser.getString(user.getBirthday(), DateParser.FORMAT_BIRTHDAY));
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, isSynchronized);
        return cv;
    }

}
