package com.reskitow.thesaferoad.db;

import com.reskitow.thesaferoad.model.User;

/**
 * Acciones que deben realizar la implementaciones de esta interfaz (Gestor de User).
 */
public interface UserManager {

    /**
     * Añade el objeto a la base de datos.
     *
     * @param user Objeto ha añadir.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean add(User user, boolean isSynchronized);

    /**
     * Actualiza el objeto de la base de datos.
     *
     * @param user Objecto ha actualizar.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean update(User user, boolean isSynchronized);

    /**
     * Elimina el objeto de la base de datos.
     *
     * @param user Objeto a eliminar.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean delete(User user);

    /**
     * Elimina el objeto de la base de datos.
     *
     * @param userId PrimaryKey del objeto User (Email).
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean delete(String userId);

    /**
     * Recupera el objeto User a través de su PrimaryKey userId.
     *
     * @param userId PrimaryKey del objeto User (Email).
     * @return El usuario si ha sido encontrado o null.
     */
    User get(String userId);

}
