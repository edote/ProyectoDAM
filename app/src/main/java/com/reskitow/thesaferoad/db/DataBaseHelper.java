package com.reskitow.thesaferoad.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Clase la cual nos proporciona la base de datos.
 */
final class DataBaseHelper extends SQLiteOpenHelper {

    private static DataBaseHelper sDataBaseHelper;

    /**
     * Método con el cual obtenemos la instancia de este objeto (patrón singleton).
     *
     * @param context Context para inicializar la base de datos.
     * @return Objeto DataBaseHelper.
     */
    static DataBaseHelper newInstance(Context context) {
        if (sDataBaseHelper == null) sDataBaseHelper = new DataBaseHelper(context);
        return sDataBaseHelper;
    }

    /**
     * Constructor privado para respetar el patrón singleton.
     */
    private DataBaseHelper(Context context) {
        super(context, InfoDB.NAME_DB, null, InfoDB.VERSION_DB);
    }

    /**
     * Primera vez que se ejecuta al obtener una base de datos y no tiene las tablas creadas.
     *
     * @param db Base de datos a utilizar.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableUsers = "CREATE TABLE " + InfoDB.TABLE_USERS + " (" + InfoDB.USER_KEY_EMAIL
                + " TEXT PRIMARY KEY, " + InfoDB.USER_KEY_FIRST_NAME + " TEXT, " + InfoDB.USER_KEY_LAST_NAME
                + " TEXT, " + InfoDB.USER_KEY_BIRTHDAY + " TEXT, " + InfoDB.KEY_IS_SYNCHRONIZED + " INTEGER)";
        String createTableRoute = "CREATE TABLE " + InfoDB.TABLE_ROUTES + " (" + InfoDB.ROUTE_KEY_ID
                + " TEXT, " + InfoDB.ROUTE_KEY_NAME + " TEXT, " + InfoDB.KEY_IS_SYNCHRONIZED + " INTEGER, "
                + InfoDB.FK_USER_ID + " TEXT, FOREIGN KEY (" + InfoDB.FK_USER_ID + ") REFERENCES "
                + InfoDB.TABLE_USERS + "(" + InfoDB.USER_KEY_EMAIL + ") ON UPDATE CASCADE, PRIMARY KEY ("
                + InfoDB.ROUTE_KEY_ID + ", " + InfoDB.FK_USER_ID + "))";
        String createTablePoint = "CREATE TABLE " + InfoDB.TABLE_POINTS + " (" + InfoDB.POINT_KEY_LATITUDE
                + " REAL, " + InfoDB.POINT_KEY_LONGITUDE + " REAL, " + InfoDB.POINT_KEY_DATE + " TEXT, "
                + InfoDB.KEY_IS_SYNCHRONIZED + " INTEGER, " + InfoDB.FK_ROUTE_ID + " TEXT, FOREIGN KEY ("
                + InfoDB.FK_ROUTE_ID + ") REFERENCES " + InfoDB.TABLE_ROUTES + "(" + InfoDB.ROUTE_KEY_ID + "))";
        String createTableTime = "CREATE TABLE " + InfoDB.TABLE_TIMES + " (" + InfoDB.TIME_KEY_START_DATE
                + " TEXT, " + InfoDB.TIME_KEY_END_DATE + " TEXT, " + InfoDB.KEY_IS_SYNCHRONIZED + " INTEGER, "
                + InfoDB.FK_ROUTE_ID + " TEXT, FOREIGN KEY (" + InfoDB.FK_ROUTE_ID + ") REFERENCES "
                + InfoDB.TABLE_ROUTES + "(" + InfoDB.ROUTE_KEY_ID + "), PRIMARY KEY (" + InfoDB.FK_ROUTE_ID + "))";
        String createTableInfo = "CREATE TABLE " + InfoDB.TABLE_INFORMATIONS + " (" + InfoDB.INFORMATION_KEY_ID
                + " TEXT, " + InfoDB.INFORMATION_KEY_TYPE + " TEXT, " + InfoDB.INFORMATION_KEY_DESCRIPTION
                + " TEXT, " + InfoDB.INFORMATION_KEY_SERIOUSNESS + " INTEGER, " + InfoDB.KEY_IS_SYNCHRONIZED
                + " INTEGER, " + InfoDB.FK_USER_ID + " TEXT, FOREIGN KEY (" + InfoDB.FK_USER_ID + ") REFERENCES "
                + InfoDB.TABLE_USERS + "(" + InfoDB.USER_KEY_EMAIL + ") ON UPDATE CASCADE, PRIMARY KEY ("
                + InfoDB.INFORMATION_KEY_ID + ", " + InfoDB.FK_USER_ID + "))";
        String createTableContact = "CREATE TABLE " + InfoDB.TABLE_CONTACTS + " (" + InfoDB.CONTACT_KEY_ID
                + " TEXT, " + InfoDB.CONTACT_KEY_FIRST_NAME + " TEXT, " + InfoDB.CONTACT_KEY_LAST_NAME
                + " TEXT, " + InfoDB.KEY_IS_SYNCHRONIZED + " INTEGER, " + InfoDB.FK_USER_ID + " TEXT, FOREIGN KEY ("
                + InfoDB.FK_USER_ID + ") REFERENCES " + InfoDB.TABLE_USERS + "(" + InfoDB.USER_KEY_EMAIL
                + ") ON UPDATE CASCADE, PRIMARY KEY (" + InfoDB.CONTACT_KEY_ID + ", " + InfoDB.FK_USER_ID + "))";
        String createTableMethods = "CREATE TABLE " + InfoDB.TABLE_METHODS + " (" + InfoDB.METHOD_KEY_ID + " TEXT, "
                + InfoDB.METHOD_KEY_TYPE + " INTEGER, " + InfoDB.METHOD_KEY_FIELD + " TEXT, " + InfoDB.KEY_IS_SYNCHRONIZED
                + " INTEGER, " + InfoDB.FK_CONTACT_ID + " TEXT, FOREIGN KEY (" + InfoDB.FK_CONTACT_ID + ") REFERENCES "
                + InfoDB.TABLE_CONTACTS + "(" + InfoDB.CONTACT_KEY_ID + ") PRIMARY KEY (" + InfoDB.METHOD_KEY_ID
                + ", " + InfoDB.FK_CONTACT_ID + "))";
        for (String query : new String[]{createTableUsers, createTableRoute, createTablePoint,
                createTableTime, createTableInfo, createTableContact, createTableMethods}) {
            db.execSQL(query);
        }
    }

    /**
     * Método que es llamado cuando la versión de la base de datos cambia.
     *
     * @param db         Base de datos a utilizar.
     * @param oldVersion Versión anterior.
     * @param newVersion Nueva version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

}
