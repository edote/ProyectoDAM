package com.reskitow.thesaferoad.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.reskitow.thesaferoad.model.Point;
import com.reskitow.thesaferoad.model.Time;
import com.reskitow.thesaferoad.utils.DateParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que realiza la implementación de RelationsRouteManager.
 */
class RelationsRouteManagerImpl implements RelationsRouteManager {

    protected DataBaseHelper mDataBaseHelper;

    RelationsRouteManagerImpl(final DataBaseHelper dataBaseHelper) {
        this.mDataBaseHelper = dataBaseHelper;
    }

    @Override
    public boolean addPoint(Point point, Object routeId, boolean isSynchronized) {
        long rowId = mDataBaseHelper.getWritableDatabase().insert(InfoDB.TABLE_POINTS, null,
                getPointContentValues(point, routeId, isSynchronized));
        return rowId != -1;
    }

    @Override
    public boolean addPoints(List<Point> points, Object routeId) {
        int pointsAdded = 0;
        for (Point point : points) {
            if (addPoint(point, routeId, true)) pointsAdded++;
        }
        return pointsAdded == points.size();
    }

    @Override
    public boolean deletePoints(Object routeId) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().delete(InfoDB.TABLE_POINTS,
                InfoDB.FK_ROUTE_ID + " = ?", new String[]{routeId.toString()});
        return rowsAffected > 0;
    }

    @Override
    public List<Point> getPoints(Object routeId) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT "
                + getPointColumnsSelect() + " FROM " + InfoDB.TABLE_POINTS
                + " WHERE " + InfoDB.FK_ROUTE_ID + " = ?", new String[]{routeId.toString()});
        return getListFromCursor(cursor);
    }

    @Override
    public boolean setPointsSynchronized(Object routeId) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_POINTS,
                getIsSynchronizedContentValues(), InfoDB.FK_ROUTE_ID + " = ?",
                new String[]{routeId.toString()});
        return rowsAffected > 0;
    }

    @Override
    public List<Point> getPointsNotSynchronized(Object routeId) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT "
                + getPointColumnsSelect() + " FROM " + InfoDB.TABLE_POINTS
                + " WHERE " + InfoDB.FK_ROUTE_ID + " = ? AND " + InfoDB.KEY_IS_SYNCHRONIZED
                + " = ?", new String[]{routeId.toString(), Integer.toString(0)});
        return getListFromCursor(cursor);
    }

    @Override
    public boolean addTime(Time time, Object routeId, boolean isSynchronized) {
        long rowId = mDataBaseHelper.getWritableDatabase().insert(InfoDB.TABLE_TIMES, null,
                getTimeContentValues(time, routeId, isSynchronized, true));
        return rowId != -1;
    }

    @Override
    public boolean updateTime(Time time, Object routeId, boolean isSynchronized) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_TIMES,
                getTimeContentValues(time, null, isSynchronized, false), InfoDB.FK_ROUTE_ID
                        + " = ?", new String[]{routeId.toString()});
        return rowsAffected > 0;
    }

    @Override
    public boolean deleteTime(Object primaryKey) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().delete(InfoDB.TABLE_TIMES,
                InfoDB.FK_ROUTE_ID + " = ?", new String[]{primaryKey.toString()});
        return rowsAffected > 0;
    }

    @Override
    public Time get(Object routeId) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT "
                        + InfoDB.TIME_KEY_START_DATE + ", " + InfoDB.TIME_KEY_END_DATE + " FROM " +
                        InfoDB.TABLE_TIMES + " WHERE " + InfoDB.FK_ROUTE_ID + " = ?",
                new String[]{routeId.toString()});
        Time time = null;
        if (cursor.moveToFirst()) {
            time = getTimeFromCursor(cursor);
        }
        cursor.close();
        return time;
    }

    private Time getTimeFromCursor(Cursor cursor) {
        return new Time(DateParser.parse(cursor.getString(0), DateParser.FORMAT_UNTIL_SECONDS),
                DateParser.parse(cursor.getString(1), DateParser.FORMAT_UNTIL_SECONDS));
    }

    @Override
    public boolean setTimeSynchronized(Object routeId) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_TIMES,
                getIsSynchronizedContentValues(), InfoDB.FK_ROUTE_ID + " = ?",
                new String[]{routeId.toString()});
        return rowsAffected > 0;
    }

    @Override
    public boolean isTimeSynchronized(Object routeId) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + InfoDB.KEY_IS_SYNCHRONIZED
                        + " FROM " + InfoDB.TABLE_TIMES + " WHERE " + InfoDB.FK_ROUTE_ID + " = ?",
                new String[]{routeId.toString()});
        boolean isSync = false;
        if (cursor.moveToFirst()) {
            isSync = cursor.getInt(0) != 0;
        }
        cursor.close();
        return isSync;
    }

    private ContentValues getPointContentValues(Point point, Object routeId, boolean isSynchronized) {
        ContentValues cv = new ContentValues(5);
        cv.put(InfoDB.POINT_KEY_LATITUDE, point.getLatitude());
        cv.put(InfoDB.POINT_KEY_LONGITUDE, point.getLongitude());
        cv.put(InfoDB.POINT_KEY_DATE, DateParser.getString(point.getDate(), DateParser.FORMAT_UNTIL_SECONDS));
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, isSynchronized);
        cv.put(InfoDB.FK_ROUTE_ID, routeId.toString());
        return cv;
    }

    private ContentValues getIsSynchronizedContentValues() {
        ContentValues cv = new ContentValues(1);
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, true);
        return cv;
    }

    private String getPointColumnsSelect() {
        return InfoDB.POINT_KEY_LATITUDE + ", " + InfoDB.POINT_KEY_LONGITUDE
                + ", " + InfoDB.POINT_KEY_DATE;
    }

    private List<Point> getListFromCursor(Cursor cursor) {
        List<Point> points = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                points.add(getPointFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return points;
    }

    private Point getPointFromCursor(Cursor cursor) {
        return new Point(cursor.getDouble(0), cursor.getDouble(1),
                DateParser.parse(cursor.getString(2), DateParser.FORMAT_UNTIL_SECONDS));
    }

    private ContentValues getTimeContentValues(Time time, Object routeId, boolean isSynchronized,
                                               boolean putForeign) {
        ContentValues cv = new ContentValues(putForeign ? 5 : 4);
        cv.put(InfoDB.TIME_KEY_START_DATE, DateParser.getString(time.getStartDate(), DateParser.FORMAT_UNTIL_SECONDS));
        cv.put(InfoDB.TIME_KEY_END_DATE, DateParser.getString(time.getEndDate(), DateParser.FORMAT_UNTIL_SECONDS));
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, isSynchronized);
        if (putForeign) {
            cv.put(InfoDB.FK_ROUTE_ID, routeId.toString());
        }
        return cv;
    }

}
