package com.reskitow.thesaferoad.db;

import android.content.Context;
import android.util.Log;

import com.reskitow.thesaferoad.model.Contact;
import com.reskitow.thesaferoad.model.Information;
import com.reskitow.thesaferoad.model.Route;

/**
 * Clase con la cual obtendremos los gestores de los objetos de la base de datos local.
 */
public final class FactoryDB {

    private static Manager<Contact> sContactManager;
    private static Manager<Route> sRouteManager;
    private static Manager<Information> sInformationManager;
    private static UserManager sUserManager;

    private static DataBaseHelper sDataBaseHelper;

    /**
     * Constructor privado para respetar el patrón singleton.
     */
    private FactoryDB() {
    }

    /**
     * Devuelve el gestor para los objetos Contact.
     *
     * @return Contact manager.
     */
    public static Manager<Contact> getContactManager() {
        if (sContactManager == null) sContactManager = new ContactManagerImpl(getDataBase());
        return sContactManager;
    }

    /**
     * Devuelve el gestor para los objetos con relación a Contact (Method).
     *
     * @return RelationContactManager manager para las relaciones de contact.
     */
    public static RelationContactManager getRelationContactManager() {
        return (RelationContactManager) getContactManager();
    }

    /**
     * Devuelve el gestor para los objetos Route.
     *
     * @return Route manager.
     */
    public static Manager<Route> getRouteManager() {
        if (sRouteManager == null) sRouteManager = new RouteManagerImpl(getDataBase());
        return sRouteManager;
    }

    /**
     * Devuelve el gestor para los objetos con relación a Route (Time y Point).
     *
     * @return RelationsRouteManager manager para las relaciones de route.
     */
    public static RelationsRouteManager getRelationsRouteManager() {
        return (RelationsRouteManager) getRouteManager();
    }

    /**
     * Devuelve el gestor para los objetos Information.
     *
     * @return Information manager.
     */
    public static Manager<Information> getInformationManager() {
        if (sInformationManager == null)
            sInformationManager = new InformationManagerImpl(getDataBase());
        return sInformationManager;
    }

    private static DataBaseHelper getDataBase() {
        if (sDataBaseHelper == null) {
            throw new IllegalStateException("Para poder inicializar los Manager's, necesitas inicializar " +
                    "la base de datos, debes llamar a init(context).");
        }
        return sDataBaseHelper;
    }

    /**
     * Devuelve el gestor para los objetos UserManager.
     *
     * @return User manager.
     */
    public static UserManager getUserManager() {
        if (sUserManager == null) {
            sUserManager = new UserManagerImpl(getDataBase());
        }
        return sUserManager;
    }

    /**
     * Inicializa la base de datos, debería llamarse al comenzar en un activity si no se ha llamado antes.
     *
     * @param context Context para inicializar la base de datos.
     */
    public static void init(Context context) {
        if (sDataBaseHelper == null) {
            sDataBaseHelper = DataBaseHelper.newInstance(context);
            Log.i("DATA_BASE", "Inicializada DB.");
        }
    }

    /**
     * Cierra la base de datos, debería llamarse en onDestroy() de las activities o servicios.
     */
    public static void close() {
        if (sDataBaseHelper != null) {
            sDataBaseHelper.close();
            Log.i("DATA_BASE", "Cerrada DB.");
        }
    }

}
