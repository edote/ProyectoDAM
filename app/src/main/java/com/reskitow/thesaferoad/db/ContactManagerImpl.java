package com.reskitow.thesaferoad.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.reskitow.thesaferoad.model.Contact;
import com.reskitow.thesaferoad.model.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que realiza la implementación de Manager con objeto Contact.
 */
class ContactManagerImpl extends RelationContactManagerImpl implements Manager<Contact> {

    ContactManagerImpl(DataBaseHelper dataBaseHelper) {
        super(dataBaseHelper);
    }

    @Override
    public boolean add(Contact contact, Object foreignKey, boolean isSynchronized) {
        long rowId = mDataBaseHelper.getWritableDatabase().insert(InfoDB.TABLE_CONTACTS, null,
                getContactContentValues(contact, foreignKey.toString(), isSynchronized, true));
        int methodsAdded = 0;
        if (rowId != -1) {
            for (Method method : contact.getMethods()) {
                if (addMethod(method, contact.getId(), isSynchronized)) methodsAdded++;
            }
        }
        return rowId != -1 && methodsAdded == contact.getMethods().size();
    }

    @Override
    public boolean add(List<Contact> contacts, Object foreignKey) {
        int contactsAdded = 0;
        for (Contact contact : contacts) {
            if (add(contact, foreignKey.toString(), true)) contactsAdded++;
        }
        return contactsAdded == contacts.size();
    }

    @Override
    public boolean update(Contact contact, Object foreignKey, boolean isSynchronized) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_CONTACTS,
                getContactContentValues(contact, null, isSynchronized, false), InfoDB.CONTACT_KEY_ID
                        + " = ? AND " + InfoDB.FK_USER_ID + " = ?", new String[]{contact.getId(), foreignKey.toString()});
        deleteMethods(contact.getId());
        for (Method method : contact.getMethods()) {
            addMethod(method, contact.getId(), isSynchronized);
        }
        return rowsAffected > 0;
    }

    @Override
    public boolean update(List<Contact> contacts, Object foreignKey) {
        int contactsUpdated = 0;
        for (Contact contact : contacts) {
            if (update(contact, foreignKey, true)) contactsUpdated++;
        }
        return contactsUpdated == contacts.size();
    }

    @Override
    public boolean delete(Object... primaryKeys) {
        deleteMethods(primaryKeys[0].toString());
        int rowsAffected = mDataBaseHelper.getWritableDatabase().delete(InfoDB.TABLE_CONTACTS,
                InfoDB.CONTACT_KEY_ID + " = ? AND " + InfoDB.FK_USER_ID + " = ?", new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        return rowsAffected > 0;
    }

    @Override
    public boolean deleteAll(Object foreignKey) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().delete(InfoDB.TABLE_CONTACTS, InfoDB.FK_USER_ID + " = ?", new String[]{foreignKey.toString()});
        return rowsAffected > 0;
    }

    @Override
    public Contact get(Object... primaryKeys) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + getContactColumnsSelect()
                + " FROM " + InfoDB.TABLE_CONTACTS + " WHERE " + InfoDB.CONTACT_KEY_ID + " = ? AND "
                + InfoDB.FK_USER_ID + " = ? ", new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        Contact contact = null;
        if (cursor.moveToFirst()) {
            contact = getContactFromCursor(cursor);
            contact.setMethods(getMethods(contact.getId()));
        }
        cursor.close();
        return contact;
    }

    @Override
    public List<Contact> getAll(Object foreignKey) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + getContactColumnsSelect()
                        + " FROM " + InfoDB.TABLE_CONTACTS + " WHERE " + InfoDB.FK_USER_ID + " = ?",
                new String[]{foreignKey.toString()});
        return getListContactsFromCursor(cursor);
    }

    private List<Contact> getListContactsFromCursor(Cursor cursor) {
        List<Contact> listContacts = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                listContacts.add(getContactFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return listContacts;
    }

    @Override
    public boolean isSynchronized(Object... primaryKeys) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + InfoDB.KEY_IS_SYNCHRONIZED + " FROM " + InfoDB.TABLE_CONTACTS +
                " WHERE " + InfoDB.CONTACT_KEY_ID + " = ? AND " + InfoDB.FK_USER_ID + " = ? ", new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        boolean isSync = false;
        if (cursor.moveToFirst()) {
            isSync = cursor.getInt(0) != 0;
        }
        cursor.close();
        return isSync;
    }

    @Override
    public boolean setSynchronized(Object... primaryKeys) {
        int rowsAffected = mDataBaseHelper.getWritableDatabase().update(InfoDB.TABLE_CONTACTS,
                getIsSynchronizedContentValues(), InfoDB.CONTACT_KEY_ID + " = ? AND " +
                        InfoDB.FK_USER_ID + " = ?", new String[]{primaryKeys[0].toString(), primaryKeys[1].toString()});
        return rowsAffected > 0;
    }

    @Override
    public List<Contact> getAllWhereIsNotSynchronized(Object foreignKey) {
        Cursor cursor = mDataBaseHelper.getReadableDatabase().rawQuery("SELECT " + getContactColumnsSelect() + " FROM " + InfoDB.TABLE_CONTACTS +
                " WHERE " + InfoDB.KEY_IS_SYNCHRONIZED + " = 0 AND " + InfoDB.FK_USER_ID + " = ?", new String[]{foreignKey.toString()});
        return getListContactsFromCursor(cursor);
    }

    private ContentValues getContactContentValues(Contact contact, String userId, boolean isSynchronized, boolean putPrimaryKeys) {
        ContentValues cv = new ContentValues(putPrimaryKeys ? 5 : 3);
        if (putPrimaryKeys) {
            cv.put(InfoDB.CONTACT_KEY_ID, contact.getId());
            cv.put(InfoDB.FK_USER_ID, userId);
        }
        cv.put(InfoDB.CONTACT_KEY_FIRST_NAME, contact.getFirstName());
        cv.put(InfoDB.CONTACT_KEY_LAST_NAME, contact.getLastName());
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, isSynchronized);
        return cv;
    }

    private String getContactColumnsSelect() {
        return InfoDB.CONTACT_KEY_ID + "," + InfoDB.CONTACT_KEY_FIRST_NAME + "," + InfoDB.CONTACT_KEY_LAST_NAME;
    }

    private Contact getContactFromCursor(Cursor cursor) {
        String contactId = cursor.getString(0);
        return new Contact(contactId, cursor.getString(1), cursor.getString(2), getMethods(contactId));
    }

    private ContentValues getIsSynchronizedContentValues() {
        ContentValues cv = new ContentValues(1);
        cv.put(InfoDB.KEY_IS_SYNCHRONIZED, true);
        return cv;
    }

}
