package com.reskitow.thesaferoad.db;

/**
 * Clase en la cual están los nombres de los campos de la BD, etc.
 */
class InfoDB {

    static final String NAME_DB = "th3s4f3r0ut3";
    static final int VERSION_DB = 1;

    static final String KEY_IS_SYNCHRONIZED = "IS_SYNCHRONIZED";

    static final String TABLE_USERS = "USERS";
    static final String USER_KEY_EMAIL = "EMAIL"; // PK
    static final String USER_KEY_FIRST_NAME = "FIRST_NAME";
    static final String USER_KEY_LAST_NAME = "LAST_NAME";
    static final String USER_KEY_BIRTHDAY = "BIRTHDAY";
    static final String FK_USER_ID = "USER_ID"; //FK

    static final String TABLE_ROUTES = "ROUTES";
    static final String ROUTE_KEY_ID = "ID"; // PK
    static final String ROUTE_KEY_NAME = "NAME";
    static final String FK_ROUTE_ID = "ROUTE_ID"; //FK

    static final String TABLE_POINTS = "POINTS";
    static final String POINT_KEY_LATITUDE = "LATITUDE";
    static final String POINT_KEY_LONGITUDE = "LONGITUDE";
    static final String POINT_KEY_DATE = "DATE";

    static final String TABLE_TIMES = "TIMES";
    static final String TIME_KEY_START_DATE = "START_DATE";
    static final String TIME_KEY_END_DATE = "END_DATE";

    static final String TABLE_CONTACTS = "CONTACTS";
    static final String CONTACT_KEY_ID = "ID"; // PK
    static final String CONTACT_KEY_FIRST_NAME = "FIRST_NAME";
    static final String CONTACT_KEY_LAST_NAME = "LAST_NAME";
    static final String FK_CONTACT_ID = "CONTACT_ID";

    static final String TABLE_METHODS = "METHODS";
    static final String METHOD_KEY_ID = "ID"; // PK
    static final String METHOD_KEY_TYPE = "TYPE";
    static final String METHOD_KEY_FIELD = "FIELD";

    static final String TABLE_INFORMATIONS = "INFORMATIONS";
    static final String INFORMATION_KEY_ID = "ID"; // PK
    static final String INFORMATION_KEY_TYPE = "TYPE";
    static final String INFORMATION_KEY_DESCRIPTION = "DESCRIPTION";
    static final String INFORMATION_KEY_SERIOUSNESS = "SERIOUSNESS";

}
