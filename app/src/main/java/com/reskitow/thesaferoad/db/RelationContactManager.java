package com.reskitow.thesaferoad.db;

import com.reskitow.thesaferoad.model.Method;

import java.util.List;

/**
 * Acciones que deben realizar la implementaciones de esta interfaz (Gestor de relaciones de contacto).
 */
public interface RelationContactManager {

    /**
     * Añade el método a la base de datos.
     *
     * @param method         Método a persistir.
     * @param contactId      Contacto con el cual estará relacionado.
     * @param isSynchronized true si está sincronizado con la bd externa, false en caso contrario.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean addMethod(Method method, Object contactId, boolean isSynchronized);

    /**
     * Añade los métodos a la base de datos.
     *
     * @param methods   Métodos a persistir.
     * @param contactId Contacto con el cual estará relacionado.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean addMethods(List<Method> methods, Object contactId);

    /**
     * Actualiza el método de la base de datos.
     *
     * @param method         Método a persistir.
     * @param contactId      Contacto con el cual estará relacionado.
     * @param isSynchronized true si está sincronizado con la bd externa, false en caso contrario.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean updateMethod(Method method, Object contactId, boolean isSynchronized);

    /**
     * Actualiza los métodos de la base de datos (true el campo isSynchronized siempre que sean listas).
     *
     * @param methods   Métodos a persistir.
     * @param contactId Contacto con el cual estará relacionado.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean updateMethods(List<Method> methods, Object contactId);

    /**
     * Elimina el método de la base de datos..
     *
     * @param primaryKeys primer índice para la primary key del objeto y el segundo para el contactID.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean deleteMethod(Object... primaryKeys);

    /**
     * Elimina todos los métodos relacionados con el contacto.
     *
     * @param contactId Contacto con el cual estará relacionado.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean deleteMethods(Object contactId);

    /**
     * Devuelve el método de la base de datos.
     *
     * @param primaryKeys primer índice para la primary key del objeto y el segundo para el contactID.
     * @return Método encontrado o null en caso contrario.
     */
    Method getMethod(Object... primaryKeys);

    /**
     * Devuelve los métodos de la base de datos.
     *
     * @param contactId Contacto con el cual estará relacionado.
     * @return Lista de métodos relacionados con el contacto.
     */
    List<Method> getMethods(Object contactId);

    /**
     * Comprueba si el método esta sincronizado.
     *
     * @param primaryKeys primer índice para la primary key del objeto y el segundo para el contactID.
     * @return true si esta sincronizado, false en caso contrario.
     */
    boolean isMethodSynchronized(Object... primaryKeys);

    /**
     * Cambia el estado del campo isSynchronized a true.
     *
     * @param primaryKeys primer índice para la primary key del objeto y el segundo para el contactID.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean setMethodSynchronized(Object... primaryKeys);

}
