package com.reskitow.thesaferoad.db;

import java.util.List;

/**
 * Objeto generico el cual se obtiene la implementación con {@link FactoryDB} para los objetos
 * {@link com.reskitow.thesaferoad.model.Route}, {@link com.reskitow.thesaferoad.model.Information} y
 * {@link com.reskitow.thesaferoad.model.Contact}, para poder realizar las acciones a la BD.
 *
 * @param <E> El objeto generico por el cual se subtituira E.
 */
public interface Manager<E> {

    /**
     * Añade el objeto a la base de datos.
     *
     * @param e              Objeto a persistir.
     * @param foreignKey     Foreign key con la que esta relacionado (userId).
     * @param isSynchronized true si se ha sincronizado con la base de datos externa, false en caso contrario.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean add(E e, Object foreignKey, boolean isSynchronized);

    /**
     * Añade los objetos a la base de datos (campo isSyncrhonized con listas es true).
     *
     * @param e          Objetos a persistir.
     * @param foreignKey Foreign key con la que esta relacionado (userId).
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean add(List<E> e, Object foreignKey);

    /**
     * Actualiza el objeto de la base de datos.
     *
     * @param e              Objeto a persistir.
     * @param foreignKey     Foreign key con la que esta relacionado (userId).
     * @param isSynchronized true si se ha sincronizado con la base de datos externa, false en caso contrario.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean update(E e, Object foreignKey, boolean isSynchronized);

    /**
     * Actualiza los objetos a la base de datos (campo isSyncrhonized con listas es true).
     *
     * @param e          Objetos a persistir.
     * @param foreignKey Foreign key con la que esta relacionado (userId).
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean update(List<E> e, Object foreignKey);

    /**
     * Eliminar el objeto de la base de datos.
     *
     * @param primaryKeys primer índice para la primary key del objeto y el segundo para el userId.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean delete(Object... primaryKeys);

    /**
     * Elimina todos los objetos relacionados con la foreign key.
     *
     * @param foreignKey Foreign key con la que esta relacionado (userId).
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean deleteAll(Object foreignKey);

    /**
     * Recupera el objeto relacionado con las primary keys.
     *
     * @param primaryKeys primer índice para la primary key del objeto y el segundo para el userId.
     * @return Objeto encontraro o null en caso contrario.
     */
    E get(Object... primaryKeys);

    /**
     * Recupera una lista de objetos relacionados con la foreignKey.
     *
     * @param foreignKey Foreign key con la que esta relacionado (userId).
     * @return Lista de objectos (nunca null).
     */
    List<E> getAll(Object foreignKey);

    /**
     * Comprueba si el objecto está sincronizado.
     *
     * @param primaryKeys primer índice para la primary key del objeto y el segundo para el userId.
     * @return true si esta sincronizado o false en caso contrario.
     */
    boolean isSynchronized(Object... primaryKeys);

    /**
     * Cambia el estado de isSynchronized a true.
     *
     * @param primaryKeys primer índice para la primary key del objeto y el segundo para el userId.
     * @return true si alguna fila ha sido afectada, false en caso contrario.
     */
    boolean setSynchronized(Object... primaryKeys);

    /**
     * Recupera una lista de objetos relacionados con la foreignKey que no esten sincronizados.
     *
     * @param foreignKey Foreign key con la que esta relacionado (userId).
     * @return Lista de objectos (nunca null).
     */
    List<E> getAllWhereIsNotSynchronized(Object foreignKey);

}
