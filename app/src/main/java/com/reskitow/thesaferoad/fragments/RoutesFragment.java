package com.reskitow.thesaferoad.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.activities.RouteDetailActivity;
import com.reskitow.thesaferoad.adapters.AdapterRoutes;
import com.reskitow.thesaferoad.asynctask.RoutesTask;
import com.reskitow.thesaferoad.callbacks.ChangeStatusServiceRoute;
import com.reskitow.thesaferoad.callbacks.RefreshDrawableRoute;
import com.reskitow.thesaferoad.model.Route;
import com.reskitow.thesaferoad.utils.Serializer;

import java.util.List;

public class RoutesFragment extends android.support.v4.app.Fragment implements AdapterRoutes.OnClickInRoute,
        RoutesTask.OnLoadRoutes, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, RefreshDrawableRoute {

    private RecyclerView mRecyclerViewRoutes;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View mEmptyState;
    private AdapterRoutes mAdapterRoutes;
    private String mUserId;
    private FloatingActionButton mButton;

    private ChangeStatusServiceRoute mCallback;

    public RoutesFragment() {
    }

    /**
     * Crea una nueva instancia de RoutesFragment.
     *
     * @param userId
     * @return la instancia de RoutesFragment con el userId establecido.
     */
    public static RoutesFragment newInstance(String userId) {
        RoutesFragment fragment = new RoutesFragment();
        Bundle bundle = new Bundle(1);
        bundle.putString(SummaryFragment.USER_ID, userId);
        fragment.setArguments(bundle);
        return fragment;
    }

    /**
     * Método onCreate que asigna el ID de usuario.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserId = getArguments().getString(SummaryFragment.USER_ID);
    }

    /**
     * Infla el layout en la vista e inicia las vistas del layout.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return la vista inflada.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_routes, container, false);
        findViews(rootView);
        configRecyclerView();
        requestRoutes();
        return rootView;
    }

    /**
     * Refresca las rutas con los valores recuperados del AsyncTask.
     */
    private void requestRoutes() {
        if (!mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(true);
        }
        new RoutesTask(this).execute(mUserId);
    }

    /**
     * /**
     * Inicializa las vistas del layout e inicia el listener del botón.
     *
     * @param rootView
     */
    private void findViews(View rootView) {
        mRecyclerViewRoutes = (RecyclerView) rootView.findViewById(R.id.recycler_view_routes);
        mEmptyState = rootView.findViewById(R.id.routes_empty_state);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView;
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mButton = (FloatingActionButton) rootView.findViewById(R.id.fab_fragment_routes);
        mButton.setOnClickListener(this);
        onChangeDrawableButtonRoute(isRunningService());
    }

    /**
     * Configura la RecyclerView estableciendo el layout y el adaptador correspondiente.
     */
    private void configRecyclerView() {
        mAdapterRoutes = new AdapterRoutes(this);
        mRecyclerViewRoutes.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerViewRoutes.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        mRecyclerViewRoutes.setHasFixedSize(true);
        mRecyclerViewRoutes.setAdapter(mAdapterRoutes);
    }

    /**
     * Pasa el userId y la ruta en bytes serializada a la otra activity.
     *
     * @param route
     * @param position
     */
    @Override
    public void onClickInRoute(Route route, int position) {
        getActivity().startActivity(new Intent(getActivity(), RouteDetailActivity.class)
                .putExtra(getString(R.string.prefs_key_email), mUserId)
                .putExtra(getString(R.string.key_intent_route_byte), Serializer.serialize(route)));
        getActivity().overridePendingTransition(0, 0);
    }

    /**
     * Si las rutas están vacías muestra el empty state, sinó muestra las rutas.
     *
     * @param routes
     */
    @Override
    public void onLoadRoutes(List<Route> routes) {
        boolean isEmpty = routes.isEmpty();
        changeUi(isEmpty);
        if (!isEmpty) mAdapterRoutes.setRoutes(routes);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Cambia la visibilidad del recycler view y del empty state.
     *
     * @param emptyState
     */
    private void changeUi(boolean emptyState) {
        mRecyclerViewRoutes.setVisibility(emptyState ? View.GONE : View.VISIBLE);
        mEmptyState.setVisibility(emptyState ? View.VISIBLE : View.GONE);
    }

    /**
     * Al refrescar, recoge las rutas de la BD.
     */
    @Override
    public void onRefresh() {
        requestRoutes();
    }

    /**
     * Recoge si el Floating Button es pulsado o no.
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_fragment_routes:
                changeStatusRouteService();
                break;
        }
    }

    /**
     * Cambia el estado del servicio.
     */
    private void changeStatusRouteService() {
        mCallback.onChangeStatusService(isRunningService());
    }

    /**
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (ChangeStatusServiceRoute) context;
    }

    /**
     * Comprueba si el servicio está corriendo o no.
     *
     * @return true si está corriendo, false si no lo está
     */
    public boolean isRunningService() {
        return PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString(getString(R.string.prefs_key_route_id), null) != null;
    }

    /**
     * Método onResume que realiza la petición de recuperar las Rutas de la BD.
     */
    @Override
    public void onResume() {
        super.onResume();
        requestRoutes();
    }

    /**
     * Cambia la imagen del botón.
     *
     * @param isRunning
     */
    @Override
    public void onChangeDrawableButtonRoute(boolean isRunning) {
        mButton.setImageResource(isRunning ? R.mipmap.ic_stop_white : R.mipmap.ic_play_arrow_white);
        Log.i("ROUTES_FRAGMENT", "onChangeDrawableButtonRoute");
    }
}
