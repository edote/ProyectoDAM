package com.reskitow.thesaferoad.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.db.UserManager;
import com.reskitow.thesaferoad.model.User;
import com.reskitow.thesaferoad.utils.DateParser;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class AccountFragment extends android.support.v4.app.Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    public interface OnSignOut {
        void onSignOut(String userId);
    }

    private String mUserId;
    private OnSignOut mOnSignOut;

    private DatePickerDialog mDatePickerDialog;
    private Calendar mCalendar;

    private TextInputLayout mFirstNameWrapper;
    private TextInputLayout mLastNameWrapper;
    private TextInputLayout mBirthdayNameWrapper;

    private UserManager mUserManager;

    public AccountFragment() {
    }

    /**
     * Crea una nueva instancia de AccountFragment.
     *
     * @param userId
     * @return AccountFragment con el id del usuario establecido.
     */
    public static AccountFragment newInstance(String userId) {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        args.putString(SummaryFragment.USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Método onCreate que asigna el ID de usuario y el calendario.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserId = getArguments().getString(SummaryFragment.USER_ID);
        mCalendar = Calendar.getInstance(TimeZone.getDefault());
    }

    /**
     * Infla el layout en la vista.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return la vista inflada
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewRoot = inflater.inflate(R.layout.fragment_account, container, false);
        findViews(viewRoot);
        putUserInUi(getUserManagerSecure().get(mUserId));
        return viewRoot;
    }

    /**
     * Inicializa las vistas del layout.
     *
     * @param viewRoot
     */
    private void findViews(View viewRoot) {
        viewRoot.findViewById(R.id.button_log_out).setOnClickListener(this);
        mFirstNameWrapper = (TextInputLayout) viewRoot.findViewById(R.id.til_first_name_account);
        mLastNameWrapper = (TextInputLayout) viewRoot.findViewById(R.id.til_last_name_account);
        mBirthdayNameWrapper = (TextInputLayout) viewRoot.findViewById(R.id.til_birthday_account);
        EditText birthday = mBirthdayNameWrapper.getEditText();
        birthday.setFocusable(false);
        birthday.setClickable(true);
        birthday.setOnClickListener(this);
        viewRoot.findViewById(R.id.button_save_account).setOnClickListener(this);
    }


    /**
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mOnSignOut = (OnSignOut) context;
    }

    /**
     * Recoge el botón pulsado y dependiendo del botón pulsado se ejecuta un método u otro.
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_log_out:
                mOnSignOut.onSignOut(mUserId);
                break;
            case R.id.et_birthday_account:
                showDatePicker();
                break;
            case R.id.button_save_account:
                updateOrInsertUser();
                break;
        }
    }

    /**
     * Obtiene los valores y los carga en los campos.
     *
     * @param user
     */
    private void putUserInUi(User user) {
        if (user != null) {
            mFirstNameWrapper.getEditText().setText(user.getFirstName());
            mLastNameWrapper.getEditText().setText(user.getLastName());
            Date birthdayDate = user.getBirthday();
            mCalendar.setTime(birthdayDate);
            String birthday = DateParser.getString(birthdayDate, DateParser.FORMAT_BIRTHDAY);
            mBirthdayNameWrapper.getEditText().setText(birthday);
        }
    }

    /**
     * Si los campos son correctos persiste el usuario en la BD.
     */
    private void updateOrInsertUser() {
        if (checkFields()) {
            persistUser();
        }
    }

    /**
     * Persiste el usuario en la BD. (Inserta o modifica dependiendo del boolean).
     */
    private void persistUser() {
        User user = getUserFromUi();
        boolean actionExecutedCorrectly = getUserManagerSecure().get(mUserId) == null ?
                getUserManagerSecure().add(user, false) : getUserManagerSecure().update(user, false);
        if (!actionExecutedCorrectly) {
            Toast.makeText(getActivity(), "ERROOOOOOOOR", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Obtiene los campos introducido por el usuario.
     *
     * @return un objeto usuario compuesto por el contenido de los campos.
     */
    private User getUserFromUi() {
        return new User(mUserId, mFirstNameWrapper.getEditText().getText().toString(),
                mLastNameWrapper.getEditText().getText().toString(), DateParser
                .parse(mBirthdayNameWrapper.getEditText().getText().toString(), DateParser.FORMAT_BIRTHDAY));
    }

    /**
     * Comprueba que los campos sean válidos no estén vacíos.
     *
     * @return
     */
    private boolean checkFields() {
        return textInputLayoutIsEmpty(mFirstNameWrapper) & textInputLayoutIsEmpty(mLastNameWrapper)
                & textInputLayoutIsEmpty(mBirthdayNameWrapper);
    }

    /**
     * Comprueba que el TextInputLayout del parámetro no esté vacío y si lo está, muestra un mensaje de error.
     *
     * @param til
     * @return true si está vacío o false sino lo está
     */
    private boolean textInputLayoutIsEmpty(TextInputLayout til) {
        boolean isEmpty = til.getEditText().getText().toString().isEmpty();
        til.setError(isEmpty ? getString(R.string.is_necessary) : null);
        til.setErrorEnabled(isEmpty);
        return !isEmpty;
    }

    /**
     * Crea y muestra el Date Picker.
     */
    private void showDatePicker() {
        if (mDatePickerDialog == null) {
            mDatePickerDialog = new DatePickerDialog(getActivity(), this, mCalendar.get(Calendar.YEAR),
                    mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
        }
        mDatePickerDialog.show();
    }

    /**
     * Establece el formato para cumpleaños.
     *
     * @param view
     * @param year
     * @param month
     * @param dayOfMonth
     */
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        mCalendar.set(Calendar.MONTH, month);
        mCalendar.set(Calendar.YEAR, year);
        mBirthdayNameWrapper.getEditText().setText(DateParser
                .getString(mCalendar.getTime(), DateParser.FORMAT_BIRTHDAY));
    }

    /**
     * Obtiene el User Manager.
     *
     * @return
     */
    private UserManager getUserManagerSecure() {
        if (mUserManager == null) mUserManager = FactoryDB.getUserManager();
        return mUserManager;
    }

}
