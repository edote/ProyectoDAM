package com.reskitow.thesaferoad.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;

import com.reskitow.thesaferoad.R;

public class PreferencesFragment extends PreferenceFragment {

    /**
     * onCreate que añade el .xml de las preferencias.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.main_preferences);
    }
}
