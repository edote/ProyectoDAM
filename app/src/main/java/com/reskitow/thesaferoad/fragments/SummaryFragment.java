package com.reskitow.thesaferoad.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.callbacks.ChangeStatusServiceRoute;
import com.reskitow.thesaferoad.callbacks.OnChangeMenuItemDrawerLayout;
import com.reskitow.thesaferoad.callbacks.RefreshButtonNotification;
import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.db.RelationContactManager;
import com.reskitow.thesaferoad.model.Contact;
import com.reskitow.thesaferoad.model.Information;
import com.reskitow.thesaferoad.model.Method;
import com.reskitow.thesaferoad.model.Point;
import com.reskitow.thesaferoad.model.Route;
import com.reskitow.thesaferoad.notification.NotificationHelper;

import java.util.ArrayList;
import java.util.List;

public class SummaryFragment extends android.support.v4.app.Fragment implements View.OnClickListener,
        RefreshButtonNotification {

    static final String USER_ID = "user_id";

    private FloatingActionButton mFab;
    private Button mButtonShowInfo;
    private String mUserId;

    private ChangeStatusServiceRoute mListener;
    private SharedPreferences mSharedPreferences;

    public SummaryFragment() {
    }

    /**
     * Crea una nueva instancia de SummaryFragment.
     *
     * @param userId
     * @return la instancia de SummaryFragment con el userId establecido.
     */
    public static SummaryFragment newInstance(String userId) {
        SummaryFragment summaryFragment = new SummaryFragment();
        Bundle bundle = new Bundle(1);
        bundle.putString(USER_ID, userId);
        summaryFragment.setArguments(bundle);
        return summaryFragment;
    }

    /**
     * Método onCreate que asigna el ID de usuario.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserId = getArguments().getString(USER_ID);
    }

    /**
     * Infla el layout en la vista e inicia las vistas del layout.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return la vista inflada
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_summary, container, false);
        setUpViews(root);
        onChangeDrawableButtonRoute(isRunningService());
        onChangeButtonNotification(isShowingNotification());
        return root;
    }

    /**
     * /**
     * Inicializa las vistas del layout e inicia el listener de show info.
     *
     * @param root
     */
    private void setUpViews(View root) {
        mButtonShowInfo = (Button) root.findViewById(R.id.button_show_information);
        mFab = (FloatingActionButton) root.findViewById(R.id.fab_fragment_summary);
        mFab.setOnClickListener(this);
        root.findViewById(R.id.button_show_routes).setOnClickListener(this);
        root.findViewById(R.id.button_show_contacts).setOnClickListener(this);
        root.findViewById(R.id.button_show_user_information).setOnClickListener(this);
        completeTextViewRoutes(getTextView(R.id.text_view_summary_routes, root));
        completeTextViewContacts(getTextView(R.id.text_view_summary_contacts, root));
        completeTextViewInformations(getTextView(R.id.text_view_summary_informations, root));
        mButtonShowInfo.setOnClickListener(this);
    }

    /**
     * Establece el último número del contacto añadido en la pantalla inicial.
     *
     * @param textView
     */
    private void completeTextViewContacts(final TextView textView) {
        if (!getNumbersContact().isEmpty()) {
            textView.setText(String.format(getString(R.string.contacts_added),
                    getNumbersContact().get(getNumbersContact().size() - 1)));
        } else {
            textView.setText(String.format(getString(R.string.contacts_added),
                    getString(R.string.without_contacts)));
        }
    }

    /**
     * Establece la última enfermedad/alergia importante en la pantalla inicial.
     *
     * @param textView
     */
    private void completeTextViewInformations(final TextView textView) {
        if (!getImportantSickness().isEmpty()) {
            textView.setText(String.format(getString(R.string.information_summary),
                    getImportantSickness().get(getImportantSickness().size() - 1)));
        } else {
            textView.setText(String.format(getString(R.string.information_summary),
                    getString(R.string.without_sickness)));
        }
    }

    /**
     * Establece en el textView el tiempo total recorrido y la distancia.
     *
     * @param textView
     */
    private void completeTextViewRoutes(final TextView textView) {
        textView.setText(String.format(getString(R.string.info_summary_routes),
                conversorTime(getTotalTime()), getTotalDistance()));
    }

    /**
     * Obtiene el textview con la id de la vista obtenido del parámetro.
     *
     * @param id
     * @param root
     * @return el Textview
     */
    private TextView getTextView(@IdRes int id, View root) {
        return ((TextView) root.findViewById(id));
    }

    /**
     * Calcula la distancia entre el punto inicial y el siguiente.
     *
     * @return la distancia en double.
     */
    private double getTotalDistance() {
        List<Route> routeList = FactoryDB.getRouteManager().getAll(mUserId);
        double distance = 0;
        if (!routeList.isEmpty()) {
            Location location = null;
            for (Point point : routeList.get(routeList.size() - 1).getPoints()) {
                if (location == null) {
                    location = new Location("point A");
                    location.setLatitude(point.getLatitude());
                    location.setLongitude(point.getLongitude());
                } else {
                    Location location2 = new Location("point B");
                    location2.setLatitude(point.getLatitude());
                    location2.setLongitude(point.getLongitude());
                    distance += location.distanceTo(location2);
                    location = location2;
                }
            }
        }
        return distance / 1000;
    }

    /**
     * Calcula el tiempo total de las rutas.
     *
     * @return el tiempo en long
     */
    private long getTotalTime() {
        List<Route> routeList = FactoryDB.getRouteManager().getAll(mUserId);
        long result = 0;
        if (!routeList.isEmpty()) {
            for (Route route : routeList) {
                result += (route.getTime().getEndDate().getTime() - route.getTime().getStartDate().getTime());
            }
        }
        return result;
    }

    /**
     * Convierte el long que le llega por parámetro en hora - minutos.
     *
     * @param time
     * @return
     */
    private String conversorTime(long time) {
        //return DateParser.getString(new Date(time),DateParser.FORMAT_DURATION);
        return (time / (60 * 60 * 1000) + " h ") + (time / (60 * 1000) + " min ");
    }

    /**
     * Obtiene los números obtenidos de cada contacto.
     *
     * @return
     */
    private List<String> getNumbersContact() {
        List<String> stringList = new ArrayList<>();
        List<Contact> contactList = FactoryDB.getContactManager().getAll(mUserId);
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        if (!contactList.isEmpty()) {
            for (Contact contact : contactList) {
                for (Method method : rcm.getMethods(contact.getId())) {
                    if (method.getType() == Method.Type.PHONE) {
                        stringList.add(method.getField());
                    }
                }
            }
        }
        return stringList;
    }

    /**
     * Obtiene todas las enfermedades importantes del usuario.
     *
     * @return
     */
    private List<String> getImportantSickness() {
        List<String> stringList = new ArrayList<>();
        List<Information> informationList = FactoryDB.getInformationManager().getAll(mUserId);
        if (!informationList.isEmpty()) {
            for (Information information : informationList) {
                if (information.getSeriousness() == 1) {
                    stringList.add(information.getDescription());
                }
            }
        }

        return stringList;
    }

    /**
     * Recoge que botón ha sido pulsado y realiza la acción correspondiente.
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_show_information:
                showInformationLockScreen();
                break;
            case R.id.fab_fragment_summary:
                changeStatusRouteService();
                break;
            case R.id.button_show_routes:
                changeMenuItem(1);
                break;
            case R.id.button_show_contacts:
                changeMenuItem(2);
                break;
            case R.id.button_show_user_information:
                changeMenuItem(3);
                break;
        }
    }

    /**
     * Posiciona el menú en la posición pasada por parámetro.
     *
     * @param positionInMenu
     */
    private void changeMenuItem(int positionInMenu) {
        ((OnChangeMenuItemDrawerLayout) getActivity()).onChangeMenuItem(positionInMenu);
    }

    /**
     * Cambia el estado del servicio.
     */
    private void changeStatusRouteService() {
        mListener.onChangeStatusService(isRunningService());
    }

    /**
     * Llama al método para crear la notificación o para eliminarla.
     */
    private void showInformationLockScreen() {
        changeNotificationFromStatusBar();
    }

    /**
     * Crea una notificación si el texto es "Mostrar" sinó la descarta.
     */
    public void changeNotificationFromStatusBar() {
        if (mButtonShowInfo.getText().equals(getString(R.string.show))) {
            if (NotificationHelper.createStandardNotification(false, getActivity(), mUserId)
                    != NotificationHelper.ID_NORMAL_NOTIFICATION) {
                NotificationHelper.createStandardNotification(false, getActivity(), mUserId);
            }
        } else if (mButtonShowInfo.getText().equals(getString(R.string.hide))) {
            NotificationHelper.cancelNotification(getActivity(), NotificationHelper
                    .createStandardNotification(false, getActivity(), mUserId));
        }
    }

    /**
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (ChangeStatusServiceRoute) context;
    }

    /**
     * Obtiene de las preferencias si el servicio está activo o no.
     *
     * @return true si el servicio está activo.
     */
    public boolean isRunningService() {
        return getPreferences().getString(getString(R.string.prefs_key_route_id), null) != null;
    }

    /**
     * Obtiene de las preferencias si la notificación está mostrándose o no.
     *
     * @return true si las notificaciones están activas.
     */
    private boolean isShowingNotification() {
        return getPreferences().getBoolean(getString(R.string.prefs_is_notification_showing), false);
    }

    /**
     * Método para cambiar la imagen del Floating Button.
     *
     * @param isRunning
     */
    @Override
    public void onChangeDrawableButtonRoute(boolean isRunning) {
        if (isAdded()) {
            mFab.setImageResource(isRunning ? R.mipmap.ic_stop_white : R.mipmap.ic_play_arrow_white);
        }
        Log.i("SUMMARY_FRAGMENT", "onChangeDrawableButtonRoute");
    }

    /**
     * Si la notificación está activa, se cambia el texto del botón "Mostrar/Ocultar".
     *
     * @param isShowing
     */
    @Override
    public void onChangeButtonNotification(boolean isShowing) {
        if (isAdded()) {
            mButtonShowInfo.setText(isShowing ? getString(R.string.hide) : getString(R.string.show));
        }
        Log.i("SUMMARY_FRAGMENT", "onChangeButtonNotification");
    }

    /**
     * Para obtener las preferencias.
     *
     * @return las preferencias
     */
    private SharedPreferences getPreferences() {
        if (mSharedPreferences == null) {
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        }
        return mSharedPreferences;
    }

}
