package com.reskitow.thesaferoad.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.activities.ContactDetailActivity;
import com.reskitow.thesaferoad.adapters.AdapterContacts;
import com.reskitow.thesaferoad.asynctask.ContactsTask;
import com.reskitow.thesaferoad.model.Contact;
import com.reskitow.thesaferoad.utils.Serializer;

import java.util.List;

public class ContactsFragment extends android.support.v4.app.Fragment implements SwipeRefreshLayout.OnRefreshListener,
        ContactsTask.LoadContacts, AdapterContacts.OnClickInContact, View.OnClickListener {

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View mEmptyState;
    private AdapterContacts mAdapterContacts;

    private String mUserId;

    public ContactsFragment() {
    }

    /**
     * Crea una nueva instancia de ContactsFragment
     *
     * @param userId
     * @return la instancia de ContactsFragment con el userId establecido.
     */
    public static ContactsFragment newInstance(String userId) {
        ContactsFragment fragment = new ContactsFragment();
        Bundle bundle = new Bundle(1);
        bundle.putString(SummaryFragment.USER_ID, userId);
        fragment.setArguments(bundle);
        return fragment;
    }

    /**
     * Método onCreate que asigna el ID de usuario.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserId = getArguments().getString(SummaryFragment.USER_ID);
    }

    /**
     * Infla el layout en la vista e inicia las vistas del layout.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewRoot = inflater.inflate(R.layout.fragment_contacts, container, false);
        findViews(viewRoot);
        configRecyclerView();
        requestContacts();
        return viewRoot;
    }

    /**
     * Configura el recyclerView con el layout correspondiente y establece el adaptador de contactos.
     */
    private void configRecyclerView() {
        mAdapterContacts = new AdapterContacts(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        mRecyclerView.setAdapter(mAdapterContacts);
    }

    /**
     * Inicializa las vistas del layout e inicia el listener de refresh.
     *
     * @param viewRoot
     */
    private void findViews(View viewRoot) {
        mRecyclerView = (RecyclerView) viewRoot.findViewById(R.id.recycler_view_contacts);
        mEmptyState = viewRoot.findViewById(R.id.contacts_empty_state);
        viewRoot.findViewById(R.id.fab_fragment_contact).setOnClickListener(this);
        mSwipeRefreshLayout = (SwipeRefreshLayout) viewRoot;
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    /**
     * Refresca los contactos.
     */
    @Override
    public void onRefresh() {
        requestContacts();
    }

    /**
     * Recupera los contactos de la BD mediante el AsyncTask.
     */
    private void requestContacts() {
        if (!mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(true);
        }
        new ContactsTask(this).execute(mUserId);
    }

    /**
     * Comprueba si los contactos están vacíos y establece la imagen de fondo. Si no está vacío,
     * muestra el contacto normal.
     *
     * @param contacts
     */
    @Override
    public void onLoadContacts(List<Contact> contacts) {
        boolean isEmpty = contacts.isEmpty();
        changeUi(isEmpty);
        if (!isEmpty) mAdapterContacts.setContacts(contacts);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Cambia la visibilidad del recycler view y del empty state.
     *
     * @param emptyState
     */
    private void changeUi(boolean emptyState) {
        mRecyclerView.setVisibility(emptyState ? View.GONE : View.VISIBLE);
        mEmptyState.setVisibility(emptyState ? View.VISIBLE : View.GONE);
    }

    /**
     * Recoge si el contacto ha sido pulsado y que contacto ha sido.
     *
     * @param contact
     * @param position
     */
    @Override
    public void onClickInContact(Contact contact, int position) {
        showContactDetail(contact);
    }

    /**
     * Recoge la pulsación en el botón "+" para añadir un contacto.
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        final int id = v.getId();
        if (id == R.id.fab_fragment_contact) {
            showContactDetail(null);
        }
    }

    /**
     * Muestra todos los contactos.
     */
    @Override
    public void onResume() {
        super.onResume();
        requestContacts();
    }

    /**
     * Muestra el contacto detalladamente.
     *
     * @param contact
     */
    private void showContactDetail(@Nullable Contact contact) {
        Intent intent = new Intent(getActivity(), ContactDetailActivity.class)
                .putExtra(getString(R.string.prefs_key_email), mUserId);
        if (contact != null) {
            intent.putExtra(getString(R.string.key_intent_contact_byte), Serializer.serialize(contact));
        }
        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
    }

}
