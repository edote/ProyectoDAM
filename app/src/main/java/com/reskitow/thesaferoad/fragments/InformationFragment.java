package com.reskitow.thesaferoad.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.adapters.AdapterInformations;
import com.reskitow.thesaferoad.asynctask.InformationsTask;
import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.db.Manager;
import com.reskitow.thesaferoad.model.Information;

import java.util.List;
import java.util.UUID;

public class InformationFragment extends android.support.v4.app.Fragment implements View.OnClickListener, AdapterInformations.OnClickInInformation,
        InformationsTask.LoadInformations, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView mRecyclerView;
    private TextInputLayout mTypeInfoWrapper;
    private TextInputLayout mDescriptionInfoWrapper;
    private Spinner mSpinnerInfo;
    private Button mButtonInsertOrUpdate;
    private Button mButtonDelete;
    private ViewGroup mEmptyState;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private Manager<Information> mInformationManager;
    private AdapterInformations mAdapterInfo;
    private String mUserId;
    private boolean isUpdating;

    public InformationFragment() {
    }

    /**
     * Crea una nueva instancia de InformationFragment.
     *
     * @param userId
     * @return la instancia de InformationFragment con el userId establecido.
     */
    public static InformationFragment newInstance(String userId) {
        InformationFragment fragment = new InformationFragment();
        Bundle args = new Bundle();
        args.putString(SummaryFragment.USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Método onCreate que asigna el ID de usuario.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserId = getArguments().getString(SummaryFragment.USER_ID);
    }

    /**
     * Infla el layout en la vista.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return la vista inflada
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewRoot = inflater.inflate(R.layout.fragment_information, container, false);
        findViews(viewRoot);
        configAdapterRecyclerView();
        requestInformations();
        return viewRoot;
    }

    /**
     * Asynctask recuperando la "información" del usuario. (Enfermedades, etc).
     */
    private void requestInformations() {
        new InformationsTask(this).execute(mUserId);
    }

    /**
     * Configura el RecyclerView y le establece el adaptador de info.
     */
    private void configAdapterRecyclerView() {
        mAdapterInfo = new AdapterInformations(getActivity(), this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        mRecyclerView.setAdapter(mAdapterInfo);
    }

    /**
     * Inicializa las vistas del layout.
     *
     * @param viewRoot
     */
    private void findViews(View viewRoot) {
        mRecyclerView = (RecyclerView) viewRoot.findViewById(R.id.recycler_view_informations);
        mTypeInfoWrapper = (TextInputLayout) viewRoot.findViewById(R.id.til_type_information);
        mDescriptionInfoWrapper = (TextInputLayout) viewRoot.findViewById(R.id.til_description_info);
        mSpinnerInfo = (Spinner) viewRoot.findViewById(R.id.spinner_info);
        mButtonInsertOrUpdate = (Button) viewRoot.findViewById(R.id.button_add_or_update_information);
        mButtonInsertOrUpdate.setOnClickListener(this);
        mButtonDelete = (Button) viewRoot.findViewById(R.id.button_delete_information);
        mButtonDelete.setOnClickListener(this);
        mEmptyState = (ViewGroup) viewRoot.findViewById(R.id.container_empty_informations);
        mSwipeRefreshLayout = (SwipeRefreshLayout) viewRoot;
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    /**
     * Obtiene el botón pulsado y inserta/modifica/elimina el objeto Information de la BD.
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        Information information = getInformationFromUi();
        switch (v.getId()) {
            case R.id.button_add_or_update_information:
                if (checkFields()) executeAction(insertOrUpdate(information));
                break;
            case R.id.button_delete_information:
                executeAction(delete(information));
                break;
        }

    }

    /**
     * Si la información que hay es nula hace la petición
     *
     * @param ok
     */
    private void executeAction(boolean ok) {
        if (ok) {
            putInformationOnUi(null);
            requestInformations();
        } else {
            Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Comprueba si los campos están vacíos.
     *
     * @return un true si es correcto o un false si no lo es
     */
    private boolean checkFields() {
        return textInputLayoutIsEmpty(mTypeInfoWrapper) &
                textInputLayoutIsEmpty(mDescriptionInfoWrapper);
    }

    /**
     * Comprueba si el campo introducido por parámetro está vacío. Si lo está muestra un texto en
     * rojo indicando que es obligatorio.
     *
     * @param til
     * @return true si está vacío y false si no.
     */
    private boolean textInputLayoutIsEmpty(TextInputLayout til) {
        boolean isEmpty = til.getEditText().getText().toString().isEmpty();
        til.setError(isEmpty ? getString(R.string.is_necessary) : null);
        til.setErrorEnabled(isEmpty);
        return !isEmpty;
    }

    /**
     * Elimina el objeto Information de la BD.
     *
     * @param information
     * @return true si ha ido bien o false si no.
     */
    private boolean delete(Information information) {
        return getInformationManagerSecure().delete(information.getId(), mUserId);
    }

    /**
     * Añade o elimina un objeto.
     *
     * @param information
     * @return true o false dependiendo de como ha ido la operación
     */
    private boolean insertOrUpdate(Information information) {
        boolean actionExecutedCorrectly;
        actionExecutedCorrectly = isUpdating ? getInformationManagerSecure().update(information, mUserId, false) :
                getInformationManagerSecure().add(information, mUserId, false);
        return actionExecutedCorrectly;
    }

    /**
     * Asigna el Manager<Information> a la variable.
     *
     * @return la variable asignada
     */
    private Manager<Information> getInformationManagerSecure() {
        if (mInformationManager == null) mInformationManager = FactoryDB.getInformationManager();
        return mInformationManager;
    }

    /**
     * Establece la información en los campos de Info y cambia el texto del botón "Añadir/Actualizar"
     * y muestra el botón de la papelera para eliminarlo.
     *
     * @param information
     */
    private void putInformationOnUi(@Nullable Information information) {
        final boolean isNull = information == null;
        isUpdating = !isNull;
        mButtonInsertOrUpdate.setTag(isNull ? null : information.getId());
        mTypeInfoWrapper.getEditText().setText(isNull ? "" : information.getType());
        mTypeInfoWrapper.setErrorEnabled(!isNull);
        mDescriptionInfoWrapper.getEditText().setText(isNull ? "" : information.getDescription());
        mDescriptionInfoWrapper.setErrorEnabled(!isNull);
        mSpinnerInfo.setSelection(isNull ? 0 : information.getSeriousness());
        mButtonDelete.setVisibility(isNull ? View.GONE : View.VISIBLE);
        mButtonInsertOrUpdate.setText(isNull ? getString(R.string.add) : getString(R.string.update));
    }

    /**
     * Cuándo se pulsa en la información, dicha información se carga en los campos correspondientes.
     *
     * @param information
     * @param position
     */
    @Override
    public void onClickInInformation(Information information, int position) {
        putInformationOnUi(information);
    }

    /**
     * Carga la información en el adaptador pero si existe la actualiza.
     *
     * @param informations
     */
    @Override
    public void onLoadInformations(List<Information> informations) {
        boolean isEmpty = informations.isEmpty();
        changeUi(isEmpty);
        if (!isEmpty) mAdapterInfo.setInformations(informations);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Cambia la visibilidad del recycler view y del empty state.
     *
     * @param emptyState
     */
    private void changeUi(boolean emptyState) {
        mRecyclerView.setVisibility(emptyState ? View.GONE : View.VISIBLE);
        mEmptyState.setVisibility(emptyState ? View.VISIBLE : View.GONE);
    }

    /**
     * Actualiza la información del layout.
     */
    @Override
    public void onRefresh() {
        if (!mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(true);
        }
        requestInformations();
    }

    /**
     * Obtiene la información de los campos y crea un nuevo Objeto Información con los
     * datos de los campos.
     *
     * @return un objeto Information con la información de los campos.
     */
    public Information getInformationFromUi() {
        Object tag = mButtonInsertOrUpdate.getTag();
        String id = tag == null ? UUID.randomUUID().toString() : tag.toString();
        return new Information(id, mTypeInfoWrapper.getEditText().getText().toString(),
                mDescriptionInfoWrapper.getEditText().getText().toString(),
                mSpinnerInfo.getSelectedItemPosition());
    }
}
