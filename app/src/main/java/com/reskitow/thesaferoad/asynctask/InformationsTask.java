package com.reskitow.thesaferoad.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.db.Manager;
import com.reskitow.thesaferoad.model.Information;

import java.util.List;

/**
 * Tarea que se ejecutará en segundo plano que recupera las informaciones de un Usuario a través de su userId;
 */
public class InformationsTask extends AsyncTask<String, String, List<Information>> {

    /**
     * Callback que deberá implementar el que quiere ejecutar la tarea para obtener los resultados.
     */
    public interface LoadInformations {

        /**
         * Método al cual es llamado una vez terminada la tarea con los resultados.
         *
         * @param informations Resultados de la búsqueda a través del userId.
         */
        void onLoadInformations(List<Information> informations);
    }

    private final LoadInformations mLoadInformations;
    private final Manager<Information> mInformationManager;

    /**
     * Construye el objeto inicializando el Information Manager (BD) y el callback al cual iran los resultados.
     *
     * @param loadInformations Callback al cual se avisara cuando termine la tarea con los resultados.
     */
    public InformationsTask(@NonNull LoadInformations loadInformations) {
        this.mLoadInformations = loadInformations;
        this.mInformationManager = FactoryDB.getInformationManager();
    }

    /**
     * Proceso que se ejecuta en segundo plano, le llega por el primer índice el userId y devuelve los resultados.
     *
     * @param params userId en la posición 0 {[0]}.
     * @return Lista de informaciones (Resultado).
     */
    @Override
    protected List<Information> doInBackground(String... params) {
        return mInformationManager.getAll(params[0]);
    }

    /**
     * Recibe los resultados y se llama al callback que se inicializa en el constructor.
     *
     * @param informations Lista de informaciones (Resultado).
     */
    @Override
    protected void onPostExecute(List<Information> informations) {
        mLoadInformations.onLoadInformations(informations);
    }
}
