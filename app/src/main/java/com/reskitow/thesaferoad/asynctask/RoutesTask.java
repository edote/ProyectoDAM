package com.reskitow.thesaferoad.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.db.Manager;
import com.reskitow.thesaferoad.model.Route;

import java.util.List;

/**
 * Tarea que se ejecutará en segundo plano que recupera las rutas de un Usuario a través de su userId;
 */
public class RoutesTask extends AsyncTask<String, String, List<Route>> {

    /**
     * Callback que deberá implementar el que quiere ejecutar la tarea para obtener los resultados.
     */
    public interface OnLoadRoutes {

        /**
         * Método al cual es llamado una vez terminada la tarea con los resultados.
         *
         * @param routes Resultados de la búsqueda a través del userId.
         */
        void onLoadRoutes(List<Route> routes);
    }

    private final OnLoadRoutes mOnLoadRoutes;
    private final Manager<Route> mManagerRoutes;

    /**
     * Construye el objeto inicializando el Route Manager (BD) y el callback al cual iran los resultados.
     *
     * @param listener Callback al cual se avisara cuando termine la tarea con los resultados.
     */
    public RoutesTask(@NonNull OnLoadRoutes listener) {
        this.mOnLoadRoutes = listener;
        this.mManagerRoutes = FactoryDB.getRouteManager();
    }

    /**
     * Proceso que se ejecuta en segundo plano, le llega por el primer índice el userId y devuelve los resultados.
     *
     * @param params userId en la posición 0 {[0]}.
     * @return Lista de rutas (Resultado).
     */
    @Override
    protected List<Route> doInBackground(String... params) {
        return mManagerRoutes.getAll(params[0]);
    }

    /**
     * Recibe los resultados y se llama al callback que se inicializa en el constructor.
     *
     * @param routes Lista de rutas (Resultado).
     */
    @Override
    protected void onPostExecute(List<Route> routes) {
        mOnLoadRoutes.onLoadRoutes(routes);
    }

}
