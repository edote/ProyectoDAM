package com.reskitow.thesaferoad.asynctask;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.db.Manager;
import com.reskitow.thesaferoad.model.Contact;

import java.util.List;

/**
 * Tarea que se ejecutará en segundo plano que recupera los contactos de un Usuario a través de su userId;
 */
public class ContactsTask extends AsyncTask<String, String, List<Contact>> {

    /**
     * Callback que deberá implementar el que quiere ejecutar la tarea para obtener los resultados.
     */
    public interface LoadContacts {

        /**
         * Método al cual es llamado una vez terminada la tarea con los resultados.
         *
         * @param contacts Resultados de la búsqueda a través del userId.
         */
        void onLoadContacts(List<Contact> contacts);
    }

    private final LoadContacts mLoadContacts;
    private final Manager<Contact> mContactManager;

    /**
     * Construye el objeto inicializando el Contact Manager (BD) y el callback al cual iran los resultados.
     *
     * @param loadContacts Callback al cual se avisara cuando termine la tarea con los resultados.
     */
    public ContactsTask(@NonNull LoadContacts loadContacts) {
        this.mLoadContacts = loadContacts;
        this.mContactManager = FactoryDB.getContactManager();
    }

    /**
     * Proceso que se ejecuta en segundo plano, le llega por el primer índice el userId y devuelve los resultados.
     *
     * @param params userId en la posición 0 {[0]}.
     * @return Lista de contactos (Resultado).
     */
    @Override
    protected List<Contact> doInBackground(String... params) {
        return mContactManager.getAll(params[0]);
    }

    /**
     * Recibe los resultados y se llama al callback que se inicializa en el constructor.
     *
     * @param contacts Lista de contactos (Resultado).
     */
    @Override
    protected void onPostExecute(List<Contact> contacts) {
        mLoadContacts.onLoadContacts(contacts);
    }
}
