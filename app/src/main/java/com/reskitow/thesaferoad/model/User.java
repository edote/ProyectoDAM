package com.reskitow.thesaferoad.model;

import java.util.Date;

public final class User extends Person {

    private String email;
    private Date birthday;

    public User(String email, String firstName, String lastName, Date birthday) {
        super(firstName, lastName);
        this.email = email;
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "User{email='" + email + '\'' + ", firstName'" + getFirstName() + '\''
                + ", lastName='" + getLastName() + '\'' + ",birthday=" + birthday + '}';
    }
}
