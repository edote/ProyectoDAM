package com.reskitow.thesaferoad.model;

public final class Information {

    private String id;
    private String type;
    private String description;
    private int seriousness;

    public Information(String id, String type, String description, int seriousness) {
        this.id = id;
        this.type = type;
        this.description = description;
        this.seriousness = seriousness;
    }

    public Information() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSeriousness() {
        return seriousness;
    }

    public void setSeriousness(int seriousness) {
        this.seriousness = seriousness;
    }

    @Override
    public String toString() {
        return "Information{id='" + id + '\'' + ", type='" + type + '\'' + ", description='"
                + description + '\'' + ", seriousness=" + seriousness + '}';
    }
}
