package com.reskitow.thesaferoad.model;

import java.io.Serializable;

public final class Method implements Serializable {

    public enum Type {
        EMAIL, PHONE
    }

    private String id;
    private Type type;
    private String field;

    public Method(String id, Type type, String field) {
        this.id = id;
        this.type = type;
        this.field = field;
    }

    public Method() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return "Method{id='" + id + '\'' + ", type=" + type + ", field='" + field + '\'' + '}';
    }
}
