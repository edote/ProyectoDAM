package com.reskitow.thesaferoad.model;

import java.util.List;

public final class Contact extends Person {

    private String id;
    private List<Method> methods;

    public Contact(String id, String firstName, String lastName, List<Method> methods) {
        super(firstName, lastName);
        this.id = id;
        this.methods = methods;
    }

    public Contact() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Method> getMethods() {
        return methods;
    }

    public void setMethods(List<Method> methods) {
        this.methods = methods;
    }

}
