package com.reskitow.thesaferoad.model;

import java.io.Serializable;
import java.util.Date;

public final class Point implements Serializable {

    private double latitude;
    private double longitude;
    private Date date;

    public Point(double latitude, double longitude, Date date) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
    }

    public Point() {
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Point{latitude=" + latitude + ", longitude=" + longitude + ", date=" + date + '}';
    }
}
