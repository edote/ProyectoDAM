package com.reskitow.thesaferoad.model;

import java.io.Serializable;
import java.util.Date;

public final class Time implements Serializable {

    private Date startDate;
    private Date endDate;

    public Time(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Time() {
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Time{startDate = " + startDate + ", endDate=" + endDate + '}';
    }
}
