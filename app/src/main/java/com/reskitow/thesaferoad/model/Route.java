package com.reskitow.thesaferoad.model;

import java.io.Serializable;
import java.util.List;

public final class Route implements Serializable {

    private String id;
    private String name;
    private Time time;
    private List<Point> points;

    public Route(String id, String name, Time time) {
        this.id = id;
        this.name = name;
        this.time = time;
    }

    public Route(String id, String name, Time time, List<Point> points) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.points = points;
    }

    public Route() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "Route{id=" + id + ", name='" + name + '\'' + ", time=" + time + ", points=" + points + '}';
    }
}
