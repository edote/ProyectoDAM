package com.reskitow.thesaferoad.services;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.db.RelationsRouteManager;
import com.reskitow.thesaferoad.model.Point;
import com.reskitow.thesaferoad.model.Route;
import com.reskitow.thesaferoad.model.Time;
import com.reskitow.thesaferoad.notification.NotificationHelper;
import com.reskitow.thesaferoad.utils.DateParser;

import java.util.Date;

public class RouteService extends RouteState implements LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private String mRouteId;
    private RelationsRouteManager mRelationsRouteManager;
    private Time mRouteTime;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;

    private boolean first;
    private int counter;
    private int counterHelp;
    private int value;

    public RouteService() {
    }

    /**
     * onCreate que inicia la BD.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        initThingsDB();
    }

    /**
     * Instancia los servicios de google y se conecta a ellos. Crea una petición de localización
     * y crea una nueva ruta.
     *
     * @param intent
     * @param flags
     * @param startId
     * @return int
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        instanceGoogleServices();
        if (!mGoogleApiClient.isConnected()) mGoogleApiClient.connect();
        createLocationRequest();
        initRoute();
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * Inicia la BD y inicia la variable "mRelationsRouteManager".
     */
    private void initThingsDB() {
        FactoryDB.init(this);
        this.mRelationsRouteManager = FactoryDB.getRelationsRouteManager();
    }

    /**
     * Inicia la ruta. Genera un nuevo objeto ruta y lo almacena en la BD.
     */
    private void initRoute() {
        Date date = new Date();
        String routeStartTime = DateParser.getString(date, DateParser.FORMAT_UNTIL_SECONDS);
        final String userId = getEmail();
        final String routeName = getString(R.string.route) + " " + routeStartTime;
        mRouteId = routeStartTime + "-" + userId;
        mRouteTime = new Time(date, date);
        Route route = new Route(mRouteId, routeName, mRouteTime, null);
        if (FactoryDB.getRouteManager().add(route, userId, false)) {
            setStartTime(date.getTime());
            setRouteId(mRouteId);
        }
    }

    /**
     * Actualiza la el objeto tiempo de la ruta.
     */
    private void updateTimeRoute() {
        Date endDate = new Date();
        if (mRouteTime == null) {
            mRouteTime = new Time(new Date(getStartTime()), endDate);
        } else {
            mRouteTime.setEndDate(endDate);
        }
        mRelationsRouteManager.updateTime(mRouteTime, getRouteId(), false);
    }

    /**
     * Instancia los servicios de Google.
     */
    private void instanceGoogleServices() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    /**
     * Crea la petición de localización y asigna el intervalo de tiempo.
     */
    protected void createLocationRequest() {
        if (Integer.parseInt(getRefreshUbicationTime()) == 0) {
            value = 2000;
        } else {
            value = 5000;
        }
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(value);
        mLocationRequest.setFastestInterval(value);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Cuándo el servicio para, se reinician las variables a null y se cierra la conexión con la BD.
     */
    @Override
    public void onDestroy() {
        updateTimeRoute();
        removeRouteId();
        removeStartTime();
        FactoryDB.close();
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Inicia las actualizaciones de las localizaciones.
     */
    @SuppressWarnings({"MissingPermission"})
    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Cuándo se conecta correctamente.
     *
     * @param bundle
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }


    /**
     * En caso de desconectarse de los servicios de google.
     *
     * @param i
     */
    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, R.string.play_services_interrump, Toast.LENGTH_SHORT).show();

    }

    /**
     * En caso de error en la conexión.
     *
     * @param connectionResult
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, R.string.gp_services_not_found, Toast.LENGTH_SHORT).show();
    }

    /**
     * Cuándo la localización cambia crea un nuevo punto y lo guatrda en la BD.
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        if (!first) {
            mLastLocation = location;
            first = true;
        }
        mRelationsRouteManager.addPoint(new Point(location.getLatitude(), location.getLongitude(),
                new Date()), mRouteId, false);
        checkSamePosition(location);
        mLastLocation = location;
    }

    /**
     * Comprueba si el punto anterior y el nuevo punto está a una distancia inferior entre ellos
     * de 10 metros a la redonda. En caso afirmativo se incrementa el contador en 1 y cuando
     * coincida dicho contador con el valor introducido en las preferencias, se lanza la
     * notificación de emergencia y llama al método para vibrar.
     *
     * @param location
     */
    private void checkSamePosition(Location location) {
        if (location.distanceTo(mLastLocation) <= 50) {
            if (counter < (Integer.parseInt(getStoppedTime()) * 60) / (value / 1000)) {
                counter++;
                System.out.println(counter);
            } else if (counter == (Integer.parseInt(getStoppedTime()) * 60) / (value / 1000)) {
                NotificationHelper.sendEmergencyNotification(this);
                checkAccident();
            }
        } else {
            NotificationHelper.cancelNotification(this, NotificationHelper.ID_EMERGENCY_NOTIFICATION);
            counter = 0;
            counterHelp = 0;
        }
    }

    /**
     * Inicia la vibración y llama al método que envía los SMS.
     */
    private void checkAccident() {
        counterHelp++;
        Vibrator v = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        v.vibrate(5000);
        if (counterHelp == 20) {
            NotificationHelper.sendSMS(this, getEmail());
        }
    }

}