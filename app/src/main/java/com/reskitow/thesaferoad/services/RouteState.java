package com.reskitow.thesaferoad.services;

import android.app.Service;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import com.reskitow.thesaferoad.R;

public abstract class RouteState extends Service {

    private SharedPreferences mPreferences;
    private String mKeyEmail;
    private String mKeyRouteId;
    private String mKeyStartTime;
    private String mUserId;

    /**
     * Carga las preferencias y asigna el mail del usuario, el id de ruta y la hora inicial a
     * sus variables correspondientes.
     */
    private void loadAll() {
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.mKeyEmail = getString(R.string.prefs_key_email);
        this.mKeyRouteId = getString(R.string.prefs_key_route_id);
        this.mKeyStartTime = getString(R.string.prefs_key_start_time);
    }

    /**
     * Comprueba si las preferencias son nulas y si lo son las carga.
     */
    private void checkPreferences() {
        if (mPreferences == null) {
            loadAll();
        }
    }

    /**
     * Obtiene el usuario de las preferencias.
     *
     * @return el email del usuario.
     */
    protected String getEmail() {
        checkPreferences();
        if (mUserId == null) mUserId = mPreferences.getString(mKeyEmail, null);
        return mUserId;
    }

    /**
     * Pone la id de ruta a null.
     *
     * @return true si se ha eliminado correctamente.
     */
    protected boolean removeRouteId() {
        return setRouteId(null);
    }

    /**
     * Obtiene el id de ruta de las preferencias.
     *
     * @return el string de id de ruta.
     */
    @Nullable
    protected String getRouteId() {
        checkPreferences();
        return mPreferences.getString(mKeyRouteId, null);
    }

    /**
     * Guarda la id de ruta en las preferencias.
     *
     * @param routeId
     * @return true si se ha añadido el string.
     */
    protected boolean setRouteId(String routeId) {
        checkPreferences();
        return mPreferences.edit().putString(mKeyRouteId, routeId).commit();
    }

    /**
     * Guarda la hora inicial en las preferencias.
     *
     * @param startTime
     * @return true si se ha añadido.
     */
    protected boolean setStartTime(long startTime) {
        checkPreferences();
        return mPreferences
                .edit()
                .putLong(mKeyStartTime, startTime)
                .commit();
    }

    /**
     * Elimina la hora inicial de las preferencias.
     *
     * @return true si se ha eliminado.
     */
    protected boolean removeStartTime() {
        checkPreferences();
        return mPreferences.edit().remove(mKeyStartTime).commit();
    }

    /**
     * Obtiene la hora inicial de las preferencias.
     *
     * @return long con el tiempo inicial.
     */
    protected long getStartTime() {
        checkPreferences();
        return mPreferences.getLong(mKeyStartTime, -1);
    }

    /**
     * Obtiene el tiempo de espera introducido por el usuario en las preferencias.
     *
     * @return el string con el número establecido en preferencias.
     */
    protected String getStoppedTime() {
        checkPreferences();
        return mPreferences.getString(getString(R.string.prefs_key_standtime), "10");
    }

    /**
     * Obtiene el tiempo de refresco introducido por el usuario en las preferencias.
     *
     * @return el string con el tiempo de refresco.
     */
    protected String getRefreshUbicationTime() {
        checkPreferences();
        return mPreferences.getString(getString(R.string.prefs_key_location), "0");
    }
}