package com.reskitow.thesaferoad.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.activities.MainActivity;
import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.db.RelationContactManager;
import com.reskitow.thesaferoad.model.Contact;
import com.reskitow.thesaferoad.model.Information;
import com.reskitow.thesaferoad.model.Method;
import com.reskitow.thesaferoad.model.Point;
import com.reskitow.thesaferoad.model.Route;
import com.reskitow.thesaferoad.model.User;
import com.reskitow.thesaferoad.utils.DateParser;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class NotificationHelper {

    public static final int ID_NORMAL_NOTIFICATION = 57667667;
    public static final int ID_EMERGENCY_NOTIFICATION = 33;
    private static MediaPlayer mMediaPlayer;

    private static NotificationManager sNotificationManager;
    private static SharedPreferences sSharedPreferences;
    private static boolean enabled;

    /**
     * Genera una notificación de "emergencia" con dos botones y la muestra con un sonido irritante.
     *
     * @param context
     * @return el id de la notificación
     */
    public static int sendEmergencyNotification(Context context) {
        Intent okIntent = new Intent(context, MainActivity.class).setAction(context.getString(R.string.notification_ok));
        Intent sosIntent = new Intent(context, MainActivity.class).setAction(context.getString(R.string.notification_sos));

        PendingIntent okPendingIntent = PendingIntent.getActivity(context, 0, okIntent, 0);
        PendingIntent sosPendingIntent = PendingIntent.getActivity(context, 0, sosIntent, 0);

        String text = context.getString(R.string.content_emergency_notification);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_help_black)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setContentTitle(context.getString(R.string.title_emergency_notification))
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setOngoing(true)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                        .addAction(R.mipmap.ic_thumb_up_white, context.getString(R.string.notification_ok), okPendingIntent)
                        .addAction(R.mipmap.ic_add_alert_white, context.getString(R.string.notification_sos), sosPendingIntent);
        if (sNotificationManager == null) {
            sNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }
        sNotificationManager.notify(ID_EMERGENCY_NOTIFICATION, mBuilder.build());
        if (!enabled) {
            enabled = true;
            enableSound(context);
        }
        startSound(context);
        return ID_EMERGENCY_NOTIFICATION;
    }

    /**
     * Mira si el usuario tiene contactos añadidos. Si tiene contactos, comprueba cada número de
     * ellos y envía un SMS a cada número con el mensaje que recoge de otr método.
     *
     * @param context
     * @param userId
     */
    public static void sendSMS(Context context, String userId) {
        SmsManager smsManager = SmsManager.getDefault();
        RelationContactManager rcm = FactoryDB.getRelationContactManager();
        for (Contact contact : FactoryDB.getContactManager().getAll(userId)) {
            List<Method> listMethod = rcm.getMethods(contact.getId());
            for (Method m : listMethod) {
                if (m.getType() == Method.Type.PHONE) {
                    smsManager.sendTextMessage(m.getField(), null, getTextMessage(context, userId), null, null);
                    Toast.makeText(context, context.getString(R.string.message_send_sms) + " " + m.getField(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    /**
     * Compreuba si el usuario tiene listas registradas y si tiene comprueba si tiene puntos.
     * Generando un mensaje que se usará para el SMS.
     *
     * @param context
     * @param user
     * @return un texto con la última ubicación o un texto pero sin ubicación.
     */
    private static String getTextMessage(Context context, String user) {
        String text = context.getString(R.string.sms_message_without_points);
        ArrayList<Route> rutas = (ArrayList) FactoryDB.getRouteManager().getAll(user);
        if (!rutas.isEmpty()) {
            ArrayList<Point> puntos = (ArrayList) FactoryDB.getRelationsRouteManager().getPoints(rutas.get(rutas.size() - 1).getId());
            if (!puntos.isEmpty()) {
                Point p = puntos.get(puntos.size() - 1);
                text = "Posible accidente en la ubicación: \n http://www.latlong.net/c/?lat=" + p.getLatitude() + "&long=" + p.getLongitude();
            }
        }
        return text;
    }

    /**
     * Crea una notificación con la información importante del usuario junto con sus enfermedades
     * en caso de que tenga.
     *
     * @param isRunning
     * @param context
     * @param userEmail
     * @return la id de la notificación
     */
    public static int createStandardNotification(boolean isRunning, Context context, String userEmail) {

        List<Information> list = getContentNotification(userEmail);
        String[] userValues = getContentUserNotification(userEmail).split("-");

        Intent stopIntent = new Intent(context, MainActivity.class).setAction(context.getString(R.string.notification_stop)).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Intent sosIntent = new Intent(context, MainActivity.class).setAction(context.getString(R.string.notification_sos)).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent stopPendingIntent = PendingIntent.getActivity(context, 0, stopIntent, 0);
        PendingIntent sosPendingIntent = PendingIntent.getActivity(context, 0, sosIntent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setOngoing(true)
                .setContentTitle(context.getString(R.string.notification_first_title))
                .addAction(R.mipmap.ic_add_alert_white, context.getString(R.string.notification_sos), sosPendingIntent)
                .addAction(R.mipmap.ic_stop_white, context.getString(R.string.notification_stop), stopPendingIntent);

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(context.getString(R.string.lock_notification_title));
        inboxStyle.setSummaryText(context.getString(R.string.app_name));
        inboxStyle.addLine(getContentToShow(userValues[0] + " " + userValues[1], userValues[2]));
        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                Information info = list.get(i);
                inboxStyle.addLine(getContentToShow(info.getType(), info.getDescription()));
            }
            mBuilder.setStyle(inboxStyle);
        } else {
            inboxStyle.addLine(context.getString(R.string.without_sickness));
            mBuilder.setStyle(inboxStyle);
        }

        if (isRunning) {
            mBuilder.setTicker(context.getString(R.string.ticker_notification));
        }
        if (sNotificationManager == null) {
            sNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }
        sNotificationManager.notify(ID_NORMAL_NOTIFICATION, mBuilder.build());
        changeNotificationPreferences(context, true);
        return ID_NORMAL_NOTIFICATION;
    }

    /**
     * Cancela una notificación con el id llegado por parámetro.
     *
     * @param context
     * @param notificationId
     */
    public static void cancelNotification(Context context, int notificationId) {
        sNotificationManager.cancel(notificationId);
        changeNotificationPreferences(context, false);
    }

    /**
     * Cambia en las preferencias el valor de "is_notification_showing".
     *
     * @param context
     * @param isRunning
     */
    private static void changeNotificationPreferences(Context context, boolean isRunning) {
        getPreferences(context).edit().putBoolean(context.getString(R.string.prefs_is_notification_showing), isRunning).apply();
    }

    /**
     * Obtiene la información del usuario pasado por parámetro.
     *
     * @param userEmail
     * @return las enfermedades/alergias del usuario.
     */
    private static List<Information> getContentNotification(String userEmail) {
        return FactoryDB.getInformationManager().getAll(userEmail);
    }

    /**
     * Obtiene toda la información del usuario (Nombre, Apellidos, Cumpleaños).
     *
     * @param userEmail
     * @return la información en un array de strings.
     */
    private static String getContentUserNotification(String userEmail) {
        User user = FactoryDB.getUserManager().get(userEmail);
        return user.getFirstName() + "-" + user.getLastName() + "-"
                + DateParser.getString(user.getBirthday(), DateParser.FORMAT_BIRTHDAY);
    }

    /**
     * Formatea la información llegada por parámetro. (Usado para la notificación standard).
     *
     * @param type
     * @param description
     * @return la información en este formato: Manolo Garcia · 29/09/1956
     */
    private static String getContentToShow(String type, String description) {
        return String.format("%s · %s", type, description);
    }

    /**
     * Devuelve las shared preferences para poder acceder a ellas.
     *
     * @param context
     * @return las preferencias.
     */
    private static SharedPreferences getPreferences(Context context) {
        if (sSharedPreferences == null) {
            sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return sSharedPreferences;
    }

    /**
     * Sube el sonido a tope del dispositivo.
     *
     * @param context
     */
    public static void enableSound(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, AudioManager.FLAG_SHOW_UI + AudioManager.FLAG_PLAY_SOUND);
    }

    /**
     * Inicia el sonido.
     *
     * @param context
     */
    private static void startSound(Context context) {
        mMediaPlayer = MediaPlayer.create(context, R.raw.alarm);
        mMediaPlayer.start();
    }

    /**
     * Para el sonido.
     */
    public static void stopSound() {
        if (mMediaPlayer != null) mMediaPlayer.stop();
    }
}