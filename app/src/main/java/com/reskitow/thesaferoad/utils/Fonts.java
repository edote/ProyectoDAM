package com.reskitow.thesaferoad.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Clase con la cual podremos obtener objetos Typeface, fuentes personalizadas a aplicar TextView's, etc.
 */
public class Fonts {

    private static Typeface sPacificoRegular;

    /**
     * Devuelve un Typeface de la tipografía pacifico-regular.
     *
     * @param context Context del cual obtendremos los Assets.
     * @return TypeFace pacifico-regular.
     */
    public static Typeface getPacificoRegular(Context context) {
        if (sPacificoRegular == null) {
            sPacificoRegular = Typeface
                    .createFromAsset(context.getAssets(), "fonts/pacifico-regular.ttf");
        }
        return sPacificoRegular;
    }

}
