package com.reskitow.thesaferoad.utils;

import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Clase con la cual podemos pasar objetos a byte[] y a la inversa.
 */
public class Serializer {

    /**
     * Serializa el objeto convirtiéndolo en byte[].
     *
     * @param object Objeto ha serializar.
     * @return Objecto serializado en byte[].
     */
    public static byte[] serialize(Object object) {
        byte[] objectInBytes;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            objectInBytes = baos.toByteArray();
        } catch (IOException e) {
            objectInBytes = null;
            e.printStackTrace();
        }
        return objectInBytes;
    }

    /**
     * Deserializa el objeto.
     *
     * @param objectInBytes Objeto en byte[].
     * @return Objeto deserializado o null si no se recuperado correctamente.
     */
    public static Object deserialize(byte[] objectInBytes) {
        Object object = null;
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(objectInBytes);
            ObjectInputStream ois = new ObjectInputStream(bais);
            object = ois.readObject();
        } catch (IOException e) {
            Log.i("SERIALIZADOR", "IOException");
        } catch (ClassNotFoundException e) {
            Log.i("SERIALIZADOR", "ClassNotFoundException");
        }
        return object;
    }

}
