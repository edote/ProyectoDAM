package com.reskitow.thesaferoad.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase con la formatearemos las fechas, tanto pasarlas de Date --> String como a la inversa.
 */
public class DateParser {

    public static final String FORMAT_UNTIL_SECONDS = "dd/MM/yyyy HH:mm:ss";
    public static final String FORMAT_BIRTHDAY = "dd/MM/yyyy";

    private static final SimpleDateFormat PARSER = new SimpleDateFormat();

    /**
     * Devuelve en forma de String la fecha pasada con el formado pasado.
     *
     * @param date    Fecha a convertir.
     * @param pattern Formato a aplicar.
     * @return String formateado.
     */
    public static String getString(Date date, String pattern) {
        PARSER.applyPattern(pattern);
        return PARSER.format(date);
    }

    /**
     * Convierte un String con el formato pasado en un Date.
     *
     * @param date    Fecha en formato String.
     * @param pattern Formato de la fecha.
     * @return Date convertida o null si ha saltado una excepción.
     */
    public static Date parse(String date, String pattern) {
        try {
            PARSER.applyPattern(pattern);
            return PARSER.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

}
