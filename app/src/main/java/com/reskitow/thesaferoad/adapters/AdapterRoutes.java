package com.reskitow.thesaferoad.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.model.Route;
import com.reskitow.thesaferoad.model.Time;
import com.reskitow.thesaferoad.utils.DateParser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Adaptador que se utiliza para un objeto RecyclerView que mostrará la lista de rutas pasadas
 * por el método setRoutes().
 */
public class AdapterRoutes extends RecyclerView.Adapter<AdapterRoutes.ViewHolder> {

    /**
     * Callback para cuando se pulse sobre una ruta.
     */
    public interface OnClickInRoute {

        /**
         * Es llamado cuando se produce la pulsación sobre la vista que contiene una Ruta.
         *
         * @param route    Ruta que se ha hecho clic.
         * @param position La posición en el array de rutas.
         */
        void onClickInRoute(Route route, int position);
    }

    private final View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int position = Integer.parseInt(v.getTag().toString());
            mOnClickInRoute.onClickInRoute(mRoutes.get(position), position);
        }
    };

    private final OnClickInRoute mOnClickInRoute;
    private final Calendar mCalendar;
    private List<Route> mRoutes;

    /**
     * Constructor con el que se construye el objeto.
     *
     * @param mOnClickInRoute Listener al cual se notificará.
     */
    public AdapterRoutes(@NonNull OnClickInRoute mOnClickInRoute) {
        this.mOnClickInRoute = mOnClickInRoute;
        this.mRoutes = new ArrayList<>();
        this.mCalendar = Calendar.getInstance();
    }

    /**
     * Crea la vista en la cual se mostrará la ruta.
     *
     * @param parent   ViewGroup al cual se añadirá.
     * @param viewType tipo de vista, de momento no lo utilizamos.
     * @return ViewHolder creado pasándole la vista creada.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.row_route_adapter, parent, false);
        view.setOnClickListener(listener);
        return new ViewHolder(view);
    }

    /**
     * Actualiza la vista con los valores de la ruta, la ruta se obtiene a través de la posición
     * que llega por parámetro.
     *
     * @param holder   ViewHolder con el cual obtendremos los diferentes TextView's, etc.
     * @param position Posición de la lista con la cual obtendremos la Ruta.
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Route route = mRoutes.get(position);
        holder.itemView.setTag(position);
        holder.mName.setText(route.getName());
        int numberPoints = route.getPoints() == null ? 0 : route.getPoints().size();
        holder.mInfoPoints.setText(String.format(holder.itemView
                .getContext().getString(R.string.number_of_points), numberPoints));
        String infoTime = getTimeInfo(route.getTime(), holder.itemView.getContext());
        holder.mInfoTime.setText(infoTime);
    }

    /**
     * Devuelve un String con la información sobre las fechas que contiene Time.
     *
     * @param time    Objeto del cual se obtienen las fechas inicio y final.
     * @param context Context con el cual obtendremos Strings.
     * @return String formateado con la información.
     */
    private String getTimeInfo(Time time, Context context) {
        String startDate = DateParser.getString(time.getStartDate(), DateParser.FORMAT_UNTIL_SECONDS);
        String endDate = DateParser.getString(time.getEndDate(), DateParser.FORMAT_UNTIL_SECONDS);
        if (startDate.equals(endDate)) {
            return context.getString(R.string.route_in_progress);
        } else if (isTheSameDay(time.getStartDate(), time.getEndDate())) {
            String[] parsed = startDate.split("\\s+");
            return String.format("%s - %s (%s)", parsed[1], endDate.split("\\s+")[1], parsed[0]);
        } else {
            return String.format("%s - %s", startDate.substring(0, startDate.length() - 3),
                    endDate.substring(0, endDate.length() - 3));
        }
    }

    /**
     * Comprueba si dos fechas són del mismo día.
     *
     * @param startDate Fecha inicio.
     * @param endDate   Fecha final.
     * @return true si són del mismo día, false en caso contrario.
     */
    private boolean isTheSameDay(Date startDate, Date endDate) {
        int year, month, day;
        mCalendar.setTime(startDate);
        year = mCalendar.get(Calendar.YEAR);
        month = mCalendar.get(Calendar.MONTH);
        day = mCalendar.get(Calendar.DAY_OF_MONTH);
        mCalendar.setTime(endDate);
        return year == mCalendar.get(Calendar.YEAR) && month == mCalendar.get(Calendar.MONTH)
                && day == mCalendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Devuelve el número total de rutas.
     *
     * @return Número de rutas en la lista.
     */
    @Override
    public int getItemCount() {
        return mRoutes.size();
    }

    /**
     * Cambia las informaciones actuales y refresca las vistas con los nuevos.
     *
     * @param mRoutes Rutas a mostrar.
     */
    public void setRoutes(List<Route> mRoutes) {
        this.mRoutes = mRoutes;
        notifyDataSetChanged();
    }

    /**
     * Clase que se encarga de obtener los elementos de la vista.
     */
    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mName;
        private final TextView mInfoPoints;
        private final TextView mInfoTime;

        /**
         * Constructor con el cual inicializa los textView's, etc.
         *
         * @param itemView Vista en la cual hará los findViewById.
         */
        ViewHolder(View itemView) {
            super(itemView);
            mName = (TextView) itemView.findViewById(R.id.text_view_route_name);
            mInfoPoints = (TextView) itemView.findViewById(R.id.text_view_info_points_route);
            mInfoTime = (TextView) itemView.findViewById(R.id.text_view_time_route);
        }

    }
}
