package com.reskitow.thesaferoad.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.model.Information;

import java.util.ArrayList;
import java.util.List;

/**
 * Adaptador que se utiliza para un objeto RecyclerView que mostrará la lista de informaciones pasadas
 * por el método setInformations().
 */
public class AdapterInformations extends RecyclerView.Adapter<AdapterInformations.ViewHolder>
        implements View.OnClickListener {

    /**
     * Callback para cuando se pulse sobre una Información.
     */
    public interface OnClickInInformation {

        /**
         * Es llamado cuando se produce la pulsación sobre la vista que contiene una información.
         *
         * @param information Información que se ha hecho clic.
         * @param position    La posición en el array de informaciones.
         */
        void onClickInInformation(Information information, int position);
    }

    private List<Information> mInformations;
    private String[] mArraySeriousness;
    private OnClickInInformation mOnClickInInformation;

    /**
     * Constructor con el que se construye el objeto.
     *
     * @param context  Context para poder obtener un array.
     * @param listener Listener al cual se notificará.
     */
    public AdapterInformations(@NonNull Context context, @NonNull OnClickInInformation listener) {
        this.mArraySeriousness = context.getResources()
                .getStringArray(R.array.types_seriouness);
        this.mInformations = new ArrayList<>();
        this.mOnClickInInformation = listener;
    }

    /**
     * Crea la vista en la cual se mostrará la información.
     *
     * @param parent   ViewGroup al cual se añadirá.
     * @param viewType tipo de vista, de momento no lo utilizamos.
     * @return ViewHolder creado pasándole la vista creada.
     */
    @Override
    public AdapterInformations.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_user_information, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.mEditButton.setOnClickListener(this);
        return viewHolder;
    }

    /**
     * Actualiza la vista con los valores de la información, la información se obtiene a través de la posición
     * que llega por parámetro.
     *
     * @param holder   ViewHolder con el cual obtendremos los diferentes TextView's, etc.
     * @param position Posición de la lista con la cual obtendremos la Información.
     */
    @Override
    public void onBindViewHolder(AdapterInformations.ViewHolder holder, int position) {
        Information info = mInformations.get(position);
        holder.mTypeInformation.setText(String.format("%s · %s", info.getType(),
                getStringFromSeriousness(info.getSeriousness())));
        holder.mDescription.setText(info.getDescription());
        holder.mEditButton.setTag(position);
    }

    /**
     * Devuelve en forma de String la importancia en int.
     *
     * @param seriousness Importancia del objeto Informacion.
     * @return String formateado.
     */
    private String getStringFromSeriousness(int seriousness) {
        return mArraySeriousness[seriousness];
    }

    /**
     * Devuelve el número total de informaciones.
     *
     * @return Número de informaciones en la lista.
     */
    @Override
    public int getItemCount() {
        return mInformations.size();
    }

    /**
     * Evento con el cual podremos notificar a nuestro callback.
     *
     * @param v Vista pulsada.
     */
    @Override
    public void onClick(View v) {
        int position = Integer.parseInt(v.getTag().toString());
        mOnClickInInformation.onClickInInformation(mInformations.get(position), position);
    }

    /**
     * Cambia las informaciones actuales y refresca las vistas con los nuevos.
     *
     * @param informations Informaciones a mostrar.
     */
    public void setInformations(List<Information> informations) {
        this.mInformations = informations;
        notifyDataSetChanged();
    }

    /**
     * Clase que se encarga de obtener los elementos de la vista.
     */
    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTypeInformation;
        private final TextView mDescription;
        private final ImageView mIcon;
        private final ViewGroup mContainer;
        private final Button mEditButton;

        /**
         * Constructor con el cual inicializa los textView's, etc.
         *
         * @param itemView Vista en la cual hará los findViewById.
         */
        ViewHolder(View itemView) {
            super(itemView);
            this.mTypeInformation = (TextView) itemView.findViewById(R.id.text_view_type_information);
            this.mDescription = (TextView) itemView.findViewById(R.id.text_view_description_info);
            this.mIcon = (ImageView) itemView.findViewById(R.id.show_more_information_icon);
            this.mContainer = (ViewGroup) itemView.findViewById(R.id.container_more_info);
            this.mEditButton = (Button) itemView.findViewById(R.id.button_edit_information);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isShowing = mContainer.isShown();
                    mContainer.setVisibility(isShowing ? View.GONE : View.VISIBLE);
                    mIcon.setImageResource(isShowing ? R.mipmap.ic_keyboard_arrow_down_black :
                            R.mipmap.ic_keyboard_arrow_up_black);
                }
            });
        }

    }
}
