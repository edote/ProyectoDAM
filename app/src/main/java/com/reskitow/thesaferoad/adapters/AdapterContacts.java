package com.reskitow.thesaferoad.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.model.Contact;
import com.reskitow.thesaferoad.model.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * Adaptador que se utiliza para un objeto RecyclerView que mostrará la lista de contactos pasados
 * por el método setContacts().
 */
public class AdapterContacts extends RecyclerView.Adapter<AdapterContacts.ViewHolder> implements
        View.OnClickListener {

    /**
     * Callback para cuando se pulse sobre un contacto.
     */
    public interface OnClickInContact {

        /**
         * Es llamado cuando se produce la pulsación sobre la vista que contiene un contacto.
         *
         * @param contact  Contacto que se ha hecho clic.
         * @param position La posición en el array de contactos.
         */
        void onClickInContact(Contact contact, int position);
    }

    private List<Contact> mContacts;
    private OnClickInContact mOnClickInContact;

    /**
     * Constructor con el que se construye el objeto.
     *
     * @param listener Listener al cual se avisará.
     */
    public AdapterContacts(@NonNull OnClickInContact listener) {
        this.mContacts = new ArrayList<>();
        this.mOnClickInContact = listener;
    }

    /**
     * Crea la vista en la cual se mostrará el contacto.
     *
     * @param parent   ViewGroup al cual se añadirá.
     * @param viewType tipo de vista, de momento no lo utilizamos.
     * @return ViewHolder creado pasándole la vista creada.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_contact_adapter, parent, false);
        root.setOnClickListener(this);
        return new ViewHolder(root);
    }

    /**
     * Actualiza la vista con los valores del contacto, el contacto se obtiene a través de la posición
     * que llega por parámetro.
     *
     * @param holder   ViewHolder con el cual obtendremos los diferentes TextView's, etc.
     * @param position Posición de la lista con la cual obtendremos el Contacto.
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contact contact = mContacts.get(position);
        holder.itemView.setTag(position);
        holder.mFirstName.setText(contact.getFirstName());
        holder.mLastName.setText(contact.getLastName());
        holder.mLinearLayout.removeAllViewsInLayout();
        for (Method method : contact.getMethods()) {
            View viewRoot = LayoutInflater.from(holder.itemView.getContext())
                    .inflate(R.layout.row_method, holder.mLinearLayout, false);
            if (method.getType() == Method.Type.EMAIL) {
                ((ImageView) viewRoot.findViewById(R.id.icon_type_contact))
                        .setImageResource(R.mipmap.ic_email_black);
            }
            ((TextView) viewRoot.findViewById(R.id.field_contact)).setText(method.getField());
            holder.mLinearLayout.addView(viewRoot);
        }
    }

    /**
     * Devuelve el número total de contactos.
     *
     * @return Número de contactos en la lista.
     */
    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    /**
     * Cambia los contactos actuales y refresca las vistas con los nuevos.
     *
     * @param mContacts Contactos a mostrar.
     */
    public void setContacts(List<Contact> mContacts) {
        this.mContacts = mContacts;
        notifyDataSetChanged();
    }

    /**
     * Devuelve los contactos de la lista.
     *
     * @return Contactos de la lista.
     */
    public List<Contact> getContacts() {
        return mContacts;
    }

    /**
     * Evento con el cual podremos notificar a nuestro callback.
     *
     * @param v Vista pulsada.
     */
    @Override
    public void onClick(View v) {
        final int position = getPositionFromTag(v);
        mOnClickInContact.onClickInContact(mContacts.get(position), position);
    }

    /**
     * Devuelve la posición de una vista.
     *
     * @param v Vista.
     * @return posición de la vista.
     */
    private int getPositionFromTag(View v) {
        return Integer.parseInt(v.getTag().toString());
    }

    /**
     * Clase que se encarga de obtener los elementos de la vista.
     */
    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mFirstName;
        private TextView mLastName;
        private ViewGroup mLinearLayout;

        /**
         * Constructor con el cual inicializa los textView's, etc.
         *
         * @param itemView Vista en la cual hará los findViewById.
         */
        ViewHolder(View itemView) {
            super(itemView);
            mFirstName = (TextView) itemView.findViewById(R.id.text_view_contact_name);
            mLastName = (TextView) itemView.findViewById(R.id.text_view_contact_last_name);
            mLinearLayout = (ViewGroup) itemView.findViewById(R.id.container_linear_contact);
        }
    }
}
