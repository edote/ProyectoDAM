package com.reskitow.thesaferoad.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.fragments.PreferencesFragment;

public class PreferencesActivity extends AppCompatActivity {

    /**
     * Método onCreate que configura la toolbar y reemplaza el fragment por el de configuración.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_settings);
        setUpToolbar();
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container_fragment_settings, new PreferencesFragment())
                .commit();
    }

    /**
     * Configura la toolbar poniendo un botón en la izquierda para retrodecer.
     */
    private void setUpToolbar() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar_preferences));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    /**
     * Cuando pulsas el retroceder simula una tecla como si pulsaras hacia atrás.
     *
     * @return devuelve true si se consume en el mismo método.
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
