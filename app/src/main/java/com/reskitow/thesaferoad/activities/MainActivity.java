package com.reskitow.thesaferoad.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.callbacks.ChangeStatusServiceRoute;
import com.reskitow.thesaferoad.callbacks.OnChangeMenuItemDrawerLayout;
import com.reskitow.thesaferoad.callbacks.RefreshButtonNotification;
import com.reskitow.thesaferoad.callbacks.RefreshDrawableRoute;
import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.fragments.AccountFragment;
import com.reskitow.thesaferoad.fragments.ContactsFragment;
import com.reskitow.thesaferoad.fragments.InformationFragment;
import com.reskitow.thesaferoad.fragments.RoutesFragment;
import com.reskitow.thesaferoad.fragments.SummaryFragment;
import com.reskitow.thesaferoad.notification.NotificationHelper;
import com.reskitow.thesaferoad.services.RouteService;
import com.reskitow.thesaferoad.utils.Fonts;

public class MainActivity extends InfoActivity implements NavigationView.OnNavigationItemSelectedListener,
        AccountFragment.OnSignOut, ChangeStatusServiceRoute, OnChangeMenuItemDrawerLayout, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final int PERMISSION_LOCATION = 33;

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private AlertDialog mAlertDialog;

    private RefreshButtonNotification mRefreshButtonNotification;
    private RefreshDrawableRoute mRefreshDrawableRoute;

    /**
     * Método on Create que inicia la BD y si no está registrado muestra la SplashScreen.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FactoryDB.init(this);
        if (!isLogged()) {
            startSplashActivity();
            return;
        } else if (!isUserInfoAdded()) {
            startUserInfoActivity();
            return;
        }
        init();
    }

    /**
     * Inicia las vistas del layout, registra los listeners en las preferencias y
     * comprueba la acción de la notificación.
     */
    private void init() {
        setUpLayout();
        checkNotification();
        registerListenerPreferences();
    }

    /**
     * Iniciamos la activity con la información del usuario.
     */
    private void startUserInfoActivity() {
        startActivity(new Intent(this, UserInfoActivity.class));
        overridePendingTransition(0, 0);
    }

    /**
     * Comprobamos si en las preferencias existe el usuario o no.
     *
     * @return
     */
    private boolean isUserInfoAdded() {
        return FactoryDB.getUserManager().get(getEmail()) != null;
    }

    /**
     * Registramos en las preferencias el listener.
     */
    private void registerListenerPreferences() {
        getPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    /**
     * Para comprobar la acción de la notificación.
     */
    private void checkNotification() {
        getActionNotification(getIntent().getAction());
    }

    /**
     * Iniciamos las Vistas, barra lateral, textviews y cargamos la información dependiendo del
     * tipo de inicio.
     */
    private void setUpLayout() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        View header = mNavigationView.getHeaderView(0);
        ((TextView) header.findViewById(R.id.text_view_logged_as)).setText(String.format(getString(R.string.logged_as),
                isLoggedAsGuest() ? getString(R.string.guest) : getEmail()));
        ((TextView) header.findViewById(R.id.text_view_title_header)).setTypeface(Fonts.getPacificoRegular(this));
        onChangeMenuItem(0);
    }

    /**
     * Método que se ejecuta cuando se pulsa hacia atrás.
     */
    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Inflamos el menú lateral.
     *
     * @param menu
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * Comprobamos que se ha pulsado el botón de arriba a la derecha para las preferencias.
     *
     * @param item
     * @return true si se lanzan las preferencias.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu_settings:
                launchActivityPreferences();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Lanzamos la activity de preferencias.
     */
    private void launchActivityPreferences() {
        startActivity(new Intent(this, PreferencesActivity.class));
        overridePendingTransition(0, 0);
    }

    /**
     * Lanza la activity de ayuda.
     */
    private void launchActivityHelp() {
    }

    /**
     * Recogemos que opción del menú lateral se ha pulsado. Lanzarememos el fragment correspondiente.
     *
     * @param item
     * @return true si no se ha asignado ningçun fragment.
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        android.support.v4.app.Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.nav_summary:
                fragment = SummaryFragment.newInstance(getEmail());
                break;
            case R.id.nav_routes:
                fragment = RoutesFragment.newInstance(getEmail());
                break;
            case R.id.nav_contacts:
                fragment = ContactsFragment.newInstance(getEmail());
                break;
            case R.id.nav_information:
                fragment = InformationFragment.newInstance(getEmail());
                break;
            case R.id.nav_acount:
                fragment = AccountFragment.newInstance(getEmail());
                break;
            case R.id.nav_settings:
                launchActivityPreferences();
                break;
            case R.id.nav_help:
                launchActivityHelp();
                break;
        }
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container_fragment_main_activity, fragment)
                    .commit();
            checkListenersPreferences(fragment);
            setTitle(item.getTitle());
            if (!item.isChecked()) item.setChecked(true);
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Para asignar los botones de "Refresh".
     *
     * @param fragment
     */
    private void checkListenersPreferences(android.support.v4.app.Fragment fragment) {
        if (fragment instanceof RefreshButtonNotification) {
            mRefreshButtonNotification = (RefreshButtonNotification) fragment;
            mRefreshDrawableRoute = (RefreshDrawableRoute) fragment;
        } else if (fragment instanceof RefreshDrawableRoute) {
            mRefreshDrawableRoute = (RefreshDrawableRoute) fragment;
        }
    }

    /**
     * Para cerrar sesión. Redirige a la pantalla inicial.
     *
     * @param userId
     */
    @Override
    public void onSignOut(String userId) {
        if (signOut()) {
            startSplashActivity();
        }
    }

    /**
     * Iniciar la pantalla inicial.
     */
    private void startSplashActivity() {
        startActivity(new Intent(this, SplashActivity.class));
        finish();
        overridePendingTransition(0, 0);
    }

    /**
     * Método "onDestroy". Cierra la BD.
     */
    @Override
    protected void onDestroy() {
        FactoryDB.close();
        unregisterListenerPreferences();
        super.onDestroy();
    }

    /**
     * "Desregistramos" los listeners de las preferencias.
     */
    private void unregisterListenerPreferences() {
        getPreferences().unregisterOnSharedPreferenceChangeListener(this);
        Log.i("ENTRO", "unregisterListenerPreferences");
    }

    /**
     * Para cambiar el estado del servicio.
     *
     * @param isRunning
     */
    @Override
    public void onChangeStatusService(boolean isRunning) {
        Log.i("ENTRO", "CAMBIO EL ESTADO DEL SERVICIO");
        Intent intent = new Intent(this, RouteService.class);
        // SI ESTA ACTIVO LO PARAMOS, SI NO, LO ARRANCAMOS.
        if (isRunning) {
            stopService(intent);
            NotificationHelper.cancelNotification(this, NotificationHelper.ID_NORMAL_NOTIFICATION);
            NotificationHelper.cancelNotification(this, NotificationHelper.ID_EMERGENCY_NOTIFICATION);
            NotificationHelper.stopSound();
        } else {
            if (checkGpsStatus()) {
                startService(intent);
                NotificationHelper.createStandardNotification(isRunning, this, getEmail());
            } else {
                alertGpsDisabled();
            }
        }
    }

    /**
     * Para comprobar que el usuario ha concedido los permisos correspondientes.
     */
    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION);
            }
        }
    }

    /**
     * Peticiones de permisos.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_LOCATION:
                if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkPermissions();
                }
                break;
        }
    }

    /**
     * Comprobar el estado del GPS.
     *
     * @return true si el proveedor está activo.
     */
    private boolean checkGpsStatus() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * Mostrar un alert dialog indicando que el GPS está desactivado.
     */
    private void alertGpsDisabled() {
        if (mAlertDialog == null) {
            mAlertDialog = new AlertDialog.Builder(this).setMessage(R.string.dialog_gps_info)
                    .setCancelable(false)
                    .setPositiveButton(R.string.confirm_dialog_gps, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent1);
                        }
                    }).create();
        }
        mAlertDialog.show();
    }

    /**
     * Para cambiar el objeto seleccionado del menú.
     *
     * @param position
     */
    @Override
    public void onChangeMenuItem(int position) {
        onNavigationItemSelected(mNavigationView.getMenu().getItem(position));
    }

    /**
     * Recoger la acción del botón pulsado en la notificación. Dependiendo del botón se realiza
     * una u otra acción.
     *
     * @param action
     */
    private void getActionNotification(String action) {
        if (action != null) {
            if (action.equals(getString(R.string.notification_sos))) {
                NotificationHelper.sendSMS(this, getEmail());
            } else if (action.equals(getString(R.string.notification_stop))) {
                onChangeStatusService(true);
                NotificationHelper.cancelNotification(this, NotificationHelper.ID_NORMAL_NOTIFICATION);
                NotificationHelper.cancelNotification(this, NotificationHelper.ID_EMERGENCY_NOTIFICATION);
            } else if (action.equals(getString(R.string.notification_ok))) {
                NotificationHelper.stopSound();
                NotificationHelper.cancelNotification(this, NotificationHelper.ID_EMERGENCY_NOTIFICATION);
            }
        }
    }

    /**
     * Comprueba si las preferencias han cambiado y llaman a otros métodos.
     *
     * @param sharedPreferences
     * @param key
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.prefs_key_route_id))) {
            notifyServiceChanged();
        } else if (key.equals(getString(R.string.prefs_is_notification_showing))) {
            notifyNotificationChanged();
        }
    }

    /**
     * Comprueba si la notificación se está mostrando.
     */
    private void notifyNotificationChanged() {
        if (mRefreshButtonNotification != null) {
            mRefreshButtonNotification.onChangeButtonNotification(getPreferences()
                    .getBoolean(getString(R.string.prefs_is_notification_showing), false));
            Log.i("THESAFEROAD", "notifyNotificationChanged...");
        }
    }

    /**
     * Comprueba el estado del servicio.
     */
    private void notifyServiceChanged() {
        if (mRefreshDrawableRoute != null) {
            mRefreshDrawableRoute.onChangeDrawableButtonRoute(getPreferences()
                    .getString(getString(R.string.prefs_key_route_id), null) != null);
            Log.i("THESAFEROAD", "notifyServiceChanged...");
        }
    }

    /**
     * Si la vista de navegación es nula, llama al primer método que inicia las vistas, comprueba notificación, etc.
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (mNavigationView == null) {
            init();
        }
    }
}
