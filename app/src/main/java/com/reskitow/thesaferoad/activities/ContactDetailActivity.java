package com.reskitow.thesaferoad.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.db.Manager;
import com.reskitow.thesaferoad.model.Contact;
import com.reskitow.thesaferoad.model.Method;
import com.reskitow.thesaferoad.utils.Serializer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ContactDetailActivity extends AppCompatActivity implements View.OnClickListener,
        ViewGroup.OnHierarchyChangeListener, DialogInterface.OnClickListener {

    private EditText mFirstName;
    private EditText mLastName;
    private TextInputLayout mFirstNameWrapper;
    private TextInputLayout mLastNameWrapper;
    private ViewGroup mViewGroupInformations;
    private View mViewInfoRow;

    private AlertDialog mDialogDelete;

    private Contact mContact;
    private String mUserId;

    /**
     * Método onCreate configura la toolbar, vistas, y actualiza la ui.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_contact);
        checkContactFromIntent();
        configToolbar();
        setUpViews();
        updateUi();
    }

    /**
     * Cambia la barra de acción dependiendo de si está modificando o añadiendo un contacto nuevo.
     */
    private void configToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(mContact == null ? getString(R.string.add_contact) : getString(R.string.update_contact));
    }

    /**
     * Método que se ejecuta cuando se pulsa atrás.
     *
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     * Recogemos si ha pulsado el botón de la papelera.
     *
     * @param item
     * @return true si se ha pulsado ese botón.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_delete:
                requestDeleteContact();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Muestra el diálogo de confirmación cuando se intenta eliminar un contacto.
     */
    private void requestDeleteContact() {
        getDialogDelete().show();
    }

    /**
     * Comprueba el contacto obteniendo los valores de un intent.
     */
    private void checkContactFromIntent() {
        mUserId = getIntent().getStringExtra(getString(R.string.prefs_key_email));
        byte[] contactInBytes = getIntent().getByteArrayExtra(getString(R.string.key_intent_contact_byte));
        if (contactInBytes != null) mContact = (Contact) Serializer.deserialize(contactInBytes);
    }

    /**
     * Infla el menú
     *
     * @param menu
     * @return true si se ha inflado.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mContact != null) {
            getMenuInflater().inflate(R.menu.actions_menu, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Actualiza los campos de contacto si son vacios, y sinó añade una nueva información.
     */
    private void updateUi() {
        invalidateOptionsMenu();
        if (mContact != null) {
            putContactInUi();
        } else {
            addRowInformation(null);
        }
    }

    /**
     * Carga los datos del contacto en los campos.
     */
    private void putContactInUi() {
        mFirstName.setText(mContact.getFirstName());
        mLastName.setText(mContact.getLastName());
        for (Method method : mContact.getMethods()) {
            addRowInformation(method);
        }
    }

    /**
     * Cargamos las vistas del layout.
     */
    private void setUpViews() {
        mFirstNameWrapper = (TextInputLayout) findViewById(R.id.til_first_name_contact);
        mFirstName = mFirstNameWrapper.getEditText();
        mLastNameWrapper = (TextInputLayout) findViewById(R.id.til_last_name_contact);
        mLastName = mLastNameWrapper.getEditText();
        mViewGroupInformations = (ViewGroup) findViewById(R.id.container_linear_methods);
        mViewInfoRow = findViewById(R.id.info_rows_contact);
        mViewGroupInformations.setOnHierarchyChangeListener(this);
        findViewById(R.id.add_method_extra).setOnClickListener(this);
        findViewById(R.id.fab_detail_contact).setOnClickListener(this);
    }

    /**
     * Recogemos el botón pulsado y realizamos la acción correspondiente al botón.
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_method_extra:
                addRowInformation(null);
                break;
            case R.id.fab_detail_contact:
                requestSaveContact();
                break;
            case R.id.remove_row_button:
                removeRow((ViewGroup) v.getParent());
                break;
        }
    }

    /**
     * Si es la primera vez que se establece el contacto, genera la ID y recoge los valores.
     * Lo guarda en la BD si los datos son válidos.
     */
    private void requestSaveContact() {
        if (checkFields()) {
            boolean add = mContact == null;
            if (add) {
                mContact = new Contact();
                mContact.setId(UUID.randomUUID().toString());
            }
            mContact.setFirstName(mFirstName.getText().toString());
            mContact.setLastName(mLastName.getText().toString());
            mContact.setMethods(getMethodsFromUi());
            persistContact(add);
        }
    }

    /**
     * Dependiendo de la condición añade o modifica el contacto indicando en pantalla como ha ido
     * la acción. (Correctamente o si se ha producido un error).
     *
     * @param add
     */
    private void persistContact(boolean add) {
        Manager<Contact> contactManager = FactoryDB.getContactManager();
        if (add) {
            boolean added = contactManager.add(mContact, mUserId, false);
            Toast.makeText(this, added ? "Añadido correctamente" : "Error al añadir", Toast.LENGTH_SHORT).show();
        } else {
            boolean updated = contactManager.update(mContact, mUserId, false);
            Toast.makeText(this, updated ? "Actualizado correctamente" : "Error al actualizar", Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    /**
     * Obtenemos los campos "Email/Teléfono" y su contenido de los campos del layout y los
     * devolvemos en un array de "Methods".
     *
     * @return
     */
    private List<Method> getMethodsFromUi() {
        final int size = mViewGroupInformations.getChildCount();
        List<Method> methods = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            View view = mViewGroupInformations.getChildAt(i);
            Method method = new Method();
            method.setId(view.getTag() == null ? UUID.randomUUID().toString() : view.getTag().toString());
            method.setField(((TextView) view.findViewById(R.id.et_field_contact)).getText().toString());
            method.setType(((Spinner) view.findViewById(R.id.spinner_type_contact)).getSelectedItemPosition() == 0 ?
                    Method.Type.EMAIL : Method.Type.PHONE);
            methods.add(method);
        }
        return methods;
    }

    /**
     * Comprueba si los campos están vacíos.
     *
     * @return un true si es correcto o un false sino lo es
     */
    private boolean checkFields() {
        return textInputLayoutIsEmpty(mFirstNameWrapper) & textInputLayoutIsEmpty(mLastNameWrapper);
    }

    /**
     * Comprueba si el campo introducido por parámetro está vacío. Si lo está muestra un texto en
     * rojo indicando que es obligatorio.
     *
     * @param til
     * @return
     */
    private boolean textInputLayoutIsEmpty(TextInputLayout til) {
        boolean isEmpty = til.getEditText().getText().toString().isEmpty();
        til.setError(isEmpty ? getString(R.string.is_necessary) : null);
        til.setErrorEnabled(isEmpty);
        return !isEmpty;
    }

    /**
     * Elimina la última fila de tipo de contacto.
     *
     * @param parent
     */
    private void removeRow(ViewGroup parent) {
        mViewGroupInformations.removeView(parent);
    }

    /**
     * Añade una línea de "contacto" (Si es tipo mail/teléfono).
     *
     * @param method
     */
    private void addRowInformation(@Nullable Method method) {
        View view = getViewRowInformation();
        if (method != null) {
            view.setTag(method.getId());
            ((EditText) view.findViewById(R.id.et_field_contact)).setText(method.getField());
            ((Spinner) view.findViewById(R.id.spinner_type_contact)).setSelection(method.getType().ordinal());
        }
        view.findViewById(R.id.remove_row_button).setOnClickListener(this);
        mViewGroupInformations.addView(view);
    }

    /**
     * Obtiene la vista de la fila, dependiendo del valor seleccionado en el spinner el teclado se
     * muestra normal o sólo números.
     *
     * @return la vista obtenida
     */
    private View getViewRowInformation() {
        View view = LayoutInflater.from(this).inflate(R.layout.row_contact_method, mViewGroupInformations, false);
        final EditText editText = (EditText) view.findViewById(R.id.et_field_contact);
        ((Spinner) view.findViewById(R.id.spinner_type_contact)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                editText.setInputType(position == 0 ? InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS :
                        InputType.TYPE_CLASS_PHONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        return view;
    }

    /**
     * Cambiamos la visibilidad a la línea para añadir un nuevo "Method".
     *
     * @param show
     */
    private void showRowInfo(boolean show) {
        mViewInfoRow.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    /**
     * Es llamado cuando el contenedor de los métodos ha añadido una vista.
     *
     * @param parent
     * @param child
     */
    @Override
    public void onChildViewAdded(View parent, View child) {
        showRowInfo(mViewGroupInformations.getChildCount() > 0);
    }

    /**
     * Es llamado cuando el contenedor de los métodos ha eliminado una vista.
     *
     * @param parent
     * @param child
     */
    @Override
    public void onChildViewRemoved(View parent, View child) {
        showRowInfo(mViewGroupInformations.getChildCount() > 0);
    }

    /**
     * Cuándo se pulsan los botones del diálogo de confirmación cuando se elimina un contacto.
     *
     * @param dialog
     * @param which
     */
    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (DialogInterface.BUTTON_POSITIVE == which) {
            boolean deleted = FactoryDB.getContactManager().delete(mContact.getId(), mUserId);
            Toast.makeText(this, deleted ? "Contacto eliminado correctamente" : "No se ha eliminado correctamente."
                    , Toast.LENGTH_SHORT).show();
            finish();
        } else {
            mDialogDelete.dismiss();
        }
    }

    /**
     * Crea y devuelve el diálogo de confirmación al eliminar un contacto.
     *
     * @return
     */
    public AlertDialog getDialogDelete() {
        if (mDialogDelete == null) {
            mDialogDelete = new AlertDialog.Builder(this)
                    .setTitle(R.string.delete_contact)
                    .setMessage(R.string.delete_contact_message)
                    .setPositiveButton(R.string.delete, this)
                    .setNegativeButton(R.string.cancel, this)
                    .setCancelable(false)
                    .create();
        }
        return mDialogDelete;
    }
}
