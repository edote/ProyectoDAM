package com.reskitow.thesaferoad.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.fragments.AccountFragment;

public class UserInfoActivity extends InfoActivity implements AccountFragment.OnSignOut {

    /**
     * onCreate reemplaza el fragment.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_fragment_user_info_activity,
                        AccountFragment.newInstance(getEmail()))
                .commit();
    }

    /**
     * Comprobamos si ha salido de su sesión y mostramos pantalla inicial.
     *
     * @param userId
     */
    @Override
    public void onSignOut(String userId) {
        if (signOut()) {
            startSplashActivity();
        }
    }

    /**
     * Lanzamos la activity inicial.
     */
    private void startSplashActivity() {
        startActivity(new Intent(this, SplashActivity.class));
        finish();
        overridePendingTransition(0, 0);
    }

    /**
     * Cuándo intenta pulsar hacia atrás, mostramos el "error" en un Toast.
     */
    @Override
    public void onBackPressed() {
        if (isUserInfoAdded()) {
            super.onBackPressed();
        } else {
            Toast.makeText(this, getString(R.string.necessary_information), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Devuelve si la información del usuario ha sido añadida o no.
     *
     * @return true si está añadido.
     */
    private boolean isUserInfoAdded() {
        return FactoryDB.getUserManager().get(getEmail()) != null;
    }

}
