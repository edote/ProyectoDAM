package com.reskitow.thesaferoad.activities;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.reskitow.thesaferoad.R;

public class InfoActivity extends AppCompatActivity {

    private SharedPreferences mPreferences;
    private String mKeyEmail;
    private String mEmailGuest;
    private String mUserId;

    /**
     * Cargamos las preferencias y establecemos el usuario invitado y el registrado.
     */
    private void loadAll() {
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.mKeyEmail = getString(R.string.prefs_key_email);
        this.mEmailGuest = getString(R.string.email_guest);
    }

    /**
     * Comprobamos si las preferencias son nulas y si lo son las cargamos.
     */
    private void checkPreferences() {
        if (mPreferences == null) {
            loadAll();
        }
    }

    /**
     * Comprobamos si el mail es nulo o no.
     *
     * @return true si está registrado.
     */
    protected boolean isLogged() {
        return getEmail() != null;
    }

    /**
     * Comprobamos si ha iniciado sesión como invitado o no.
     *
     * @return true si se ha registrado como guest
     */
    protected boolean isLoggedAsGuest() {
        return getEmail().equalsIgnoreCase(mEmailGuest);
    }

    /**
     * Obtenemos el email de las preferencias.
     *
     * @return el usuario
     */
    protected String getEmail() {
        checkPreferences();
        if (mUserId == null) mUserId = mPreferences.getString(mKeyEmail, null);
        return mUserId;
    }

    /**
     * Guardamos en la base de datos el mail invitado.
     *
     * @return true si se ha guardado OK.
     */
    protected boolean signInAsGuest() {
        return signIn(mEmailGuest);
    }

    /**
     * Cuándo el usuario inicia sesión las preferencias se introduce el usuario.
     *
     * @param email
     * @return true si se ha insertado OK.
     */
    protected boolean signIn(String email) {
        checkPreferences();
        return mPreferences
                .edit()
                .putString(mKeyEmail, email)
                .commit();
    }

    /**
     * Cambiamos a null el usuario en las preferencias. (Salir de tu sesión).
     *
     * @return true si se ha insertado Ok.
     */
    protected boolean signOut() {
        return signIn(null);
    }

    /**
     * Obtenemos las preferencias.
     *
     * @return las shared preferences.
     */
    protected SharedPreferences getPreferences() {
        checkPreferences();
        return mPreferences;
    }

}
