package com.reskitow.thesaferoad.activities;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.db.FactoryDB;
import com.reskitow.thesaferoad.model.Point;
import com.reskitow.thesaferoad.model.Route;
import com.reskitow.thesaferoad.model.Time;
import com.reskitow.thesaferoad.utils.DateParser;
import com.reskitow.thesaferoad.utils.Serializer;

import java.util.List;
import java.util.Random;

public class RouteDetailActivity extends AppCompatActivity implements View.OnClickListener,
        DialogInterface.OnClickListener, OnMapReadyCallback {

    private Route mRoute;
    private String mUserId;

    private AlertDialog mDialogName;
    private TextInputLayout mNameRouteWrapper;
    private ViewGroup mContainerInfoPoints;
    private GoogleMap mGoogleMap;

    private AlertDialog mDialogDelete;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;

    /**
     * Método onCreate que configura la toolbar, etc.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_detail);
        setUpToolbar();
        checkRouteFromIntent();
        findViews();
        updateUi();
    }

    /**
     * Método que ejecuta cuando se pulsa atrás.
     *
     * @return true
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     * Inflamos el menú lateral.
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actions_menu, menu);
        return true;
    }

    /**
     * Obtenemos el botón pulsado y llamamos al método que mostrará el diálogo.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_delete:
                requestDeleteRoute();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Obtenemos y mostramos el AlertDialog.
     */
    private void requestDeleteRoute() {
        getDialogDelete().show();
    }

    /**
     * Crea el AlertDialog para confirmar la eliminación de ruta.
     *
     * @return el AlertDialog generado.
     */
    private AlertDialog getDialogDelete() {
        if (mDialogDelete == null) {
            mDialogDelete = new AlertDialog.Builder(this)
                    .setTitle(R.string.delete_route)
                    .setMessage(R.string.delete_route_message)
                    .setPositiveButton(R.string.delete, this)
                    .setNegativeButton(R.string.cancel, this)
                    .setCancelable(false)
                    .create();
        }
        return mDialogDelete;
    }

    /**
     * Actualizamos el título de la ruta.
     */
    private void updateUi() {
        updateTitle();
    }

    /**
     * Inicializamos las vistas del layout y cargamos la información de la ruta.
     */
    private void findViews() {
        Time time = mRoute.getTime();
        String format = DateParser.FORMAT_UNTIL_SECONDS;
        mNameRouteWrapper = (TextInputLayout) findViewById(R.id.til_name_route);
        ((TextView) findViewById(R.id.start_date_route_detail)).setText(DateParser.getString(time.getStartDate(), format));
        ((TextView) findViewById(R.id.end_date_route_detail)).setText(DateParser.getString(time.getEndDate(), format));
        mContainerInfoPoints = (ViewGroup) findViewById(R.id.container_linear_points);
        final List<Point> points = mRoute.getPoints();
        ((TextView) findViewById(R.id.text_view_info_total_points)).setText(String.format(getString(R.string.total_of_points), points != null ? points.size() : 0));
        addRowsOfPoints(points);
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_detail_route);
        if (points == null || points.isEmpty()) {
            getFragmentManager().beginTransaction().hide(mapFragment).commit();

            findViewById(R.id.image_view_desert).setVisibility(View.VISIBLE);
        } else {
            mapFragment.getMapAsync(this);
        }
        findViewById(R.id.fab_detail_route).setOnClickListener(this);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
    }

    /**
     * Comprueba si la lista de puntos esta vacía o no para llamar a los otros métodos.
     *
     * @param points
     */
    private void addRowsOfPoints(List<Point> points) {
        if (points == null || points.isEmpty()) {
            addRowPoint(null);
        } else {
            for (Point point : points) {
                addRowPoint(point);
            }
        }
    }

    /**
     * Mostramos el TextView con los puntos registrados.
     *
     * @param point
     */
    private void addRowPoint(@Nullable Point point) {
        View view = getViewRowPoint();
        TextView textView = (TextView) view.findViewById(R.id.text_view_info_point);
        if (point == null) {
            textView.setText(getString(R.string.no_points));
        } else {
            textView.setText(getStringPointHtml(point));
        }
        mContainerInfoPoints.addView(view);
    }

    /**
     * Formateamos la información de un punto y la devolvemos.
     *
     * @param point
     * @return
     */
    private String getStringPointHtml(Point point) {
        return String.format(getString(R.string.info_point), DateParser.getString(point.getDate(),
                DateParser.FORMAT_UNTIL_SECONDS), point.getLatitude(), point.getLongitude());
    }

    /**
     * Recupera el mail de las SharedPreferences y obtiene la ruta en bytes.
     */
    private void checkRouteFromIntent() {
        mUserId = getIntent().getStringExtra(getString(R.string.prefs_key_email));
        mRoute = (Route) Serializer.deserialize(getIntent()
                .getByteArrayExtra(getString(R.string.key_intent_route_byte)));
        if (mGoogleMap != null) onMapReady(mGoogleMap);
    }

    /**
     * Establece la barra de superior.
     */
    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    /**
     * Método que recoge el click del botón para modificar el nombre de la ruta.
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_detail_route:
                requestChangeName();
                break;
        }
    }

    /**
     * Muestra el diálogo para cambiar el nombre.
     */
    private void requestChangeName() {
        getDialogChangeName().show();
    }

    /**
     * Crea el diálogo para cambiar el nombre.
     *
     * @return el AlertDialog generado.
     */
    private AlertDialog getDialogChangeName() {
        if (mDialogName == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.view_alert_dialog_name_route, null);
            mNameRouteWrapper = (TextInputLayout) view.findViewById(R.id.til_name_route);
            mDialogName = new AlertDialog.Builder(this)
                    .setTitle(R.string.change_name)
                    .setView(view)
                    .setPositiveButton(R.string.change, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            changeNameRoute();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setCancelable(false)
                    .create();
            mNameRouteWrapper.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    Button button = mDialogName.getButton(DialogInterface.BUTTON_POSITIVE);
                    if (button != null) button.setEnabled(!TextUtils.isEmpty(s));
                }
            });
        }
        mNameRouteWrapper.getEditText().setText(mRoute.getName());
        mNameRouteWrapper.requestFocus();
        return mDialogName;
    }

    /**
     * Obtenemos el nombre de la ruta y lo actualizamos.
     */
    private void changeNameRoute() {
        mRoute.setName(mNameRouteWrapper.getEditText().getText().toString());
        FactoryDB.getRouteManager().update(mRoute, mUserId, false);
        updateTitle();
    }

    /**
     * Actualizamos la barra con el título de la ruta.
     */
    private void updateTitle() {
        mCollapsingToolbarLayout.setTitle(mRoute.getName());
    }

    /**
     * Inflamos la vista con el layout.
     *
     * @return ls vista inflada
     */
    public View getViewRowPoint() {
        return LayoutInflater.from(this).inflate(R.layout.row_point, mContainerInfoPoints, false);
    }

    /**
     * Método al cual se accede cuando el mapa está cargado. Genera los puntos con las lineas correspondientes.
     *
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mGoogleMap = googleMap;
        if (mRoute != null) {
            PolylineOptions polylineOptions = new PolylineOptions();
            LatLng latLng = null;
            if (!mRoute.getPoints().isEmpty()) {
                List<Point> points = mRoute.getPoints();
                for (Point point : points) {
                    latLng = new LatLng(point.getLatitude(), point.getLongitude());
                    polylineOptions.add(latLng);
                }
                mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(points.get(0).getLatitude(), points.get(0).getLongitude())));
                if (points.size() > 1) {
                    mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(points.get(points.size() - 1).getLatitude(), points.get(points.size() - 1).getLongitude()))
                            .icon(BitmapDescriptorFactory
                                    .defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                }
                if (latLng != null)
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                final Random random = new Random();
                final int color = Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256));
                mGoogleMap.addPolyline(polylineOptions.color(color));
            }
        }
    }

    /**
     * Obtenemos el botón pulsado del diálogo. Dependiendo del botón se elimina o se cancela.
     *
     * @param dialog
     * @param which
     */
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                boolean removed = FactoryDB.getRouteManager().delete(mRoute.getId(), mUserId);
                if (removed) finish();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                dialog.dismiss();
                break;
        }
    }
}