package com.reskitow.thesaferoad.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.reskitow.thesaferoad.R;
import com.reskitow.thesaferoad.utils.Fonts;

public class SplashActivity extends InfoActivity implements View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions mGoogleSignInOptions;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        findsViews();
        start();
        instanceGoogleSignInOptions();
        createGoogleApiClient();
    }

    /**
     * Buscamos el id de los botones y le establecemos el listener a cada uno de ellos para saber
     * si ha sido pulsado.
     */
    private void findsViews() {
        findViewById(R.id.guest_button).setOnClickListener(this);
        findViewById(R.id.sign_in_button).setOnClickListener(this);
    }

    /**
     * Lanza la activity inicial con un intent y cierra la SplashActivity.
     */
    private void launchSummaryActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        //overridePendingTransition(0, 0);
    }

    /**
     * Comprueba si ya ha iniciado sesión o no. Si ya ha iniciado, llama al método anterior
     * para iniciar la activity inicial.
     */
    private void start() {
        if (isLogged()) {
            launchSummaryActivity();
        }
    }

    /**
     * Recogemos el botón pulsado y realizamos las distintas acciones según el botón.
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                requestSignInWithGoogle();
                break;
            case R.id.guest_button:
                if (signInAsGuest()) launchSummaryActivity();
                break;
            default:
        }
    }

    /**
     * Lanza la petición para iniciar sesión con Google.
     */
    private void requestSignInWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    /**
     * Instanciamos las opciones de inicio de sesión de Google.
     */
    private void instanceGoogleSignInOptions() {
        mGoogleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
    }

    /**
     * Creamos la Google Api Client.
     */
    private void createGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, mGoogleSignInOptions)
                .build();
    }

    /**
     * Método que se ejecuta cuando la activity ya ha sido lanzada y obtenemos mediante
     * el intent, el result de la api de google, la cual pasaremos por parámetro a otro método.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    /**
     * Le llega un GoogleSignInResult por parámetro del cual obtendremos el email y se lo
     * pasamos a un método que lo guardará en las SharedPreferences.
     *
     * @param result
     */
    private void handleSignInResult(GoogleSignInResult result) {
        signIn(result.getSignInAccount().getEmail());
        start();
    }

    /**
     * Cuándo la conexión con la API no ha sido satisfactoria, se llama a este método que muestra
     * un mensaje en forma de Toast indicando el error.
     *
     * @param connectionResult
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Fallo de creacion de Api de Google", Toast.LENGTH_SHORT).show();
    }
}