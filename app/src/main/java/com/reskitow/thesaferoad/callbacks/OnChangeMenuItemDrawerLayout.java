package com.reskitow.thesaferoad.callbacks;

/**
 * Definición de la interfaz que será invocada cuando el menú quiera ser cambiado.
 */
public interface OnChangeMenuItemDrawerLayout {

    /**
     * Llamado cuando el estado del menú ha sido cambiado.
     *
     * @param position posición del menú a cambiar (basado en 0 el primero).
     */
    void onChangeMenuItem(int position);

}
