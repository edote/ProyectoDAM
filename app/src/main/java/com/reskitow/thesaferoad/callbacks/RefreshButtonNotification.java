package com.reskitow.thesaferoad.callbacks;

/**
 * Definición de la interfaz que será invocada cuando el estado de la notificación sea cambiada.
 */
public interface RefreshButtonNotification extends RefreshDrawableRoute {

    /**
     * Llamado cuando el estado de la notificación cambia.
     *
     * @param isShowing true si la notificación está activa, falso en caso contrario.
     */
    void onChangeButtonNotification(boolean isShowing);

}
