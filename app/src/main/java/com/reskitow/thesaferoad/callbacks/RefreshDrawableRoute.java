package com.reskitow.thesaferoad.callbacks;

/**
 * Definición de la interfaz que será invocada cuando el estado del servicio sea cambiado.
 */
public interface RefreshDrawableRoute {

    /**
     * Llamado cuando el estado del servicio cambia.
     *
     * @param isRunning true si el servicio de la ruta está corriendo, false en caso contrario.
     */
    void onChangeDrawableButtonRoute(boolean isRunning);

}
