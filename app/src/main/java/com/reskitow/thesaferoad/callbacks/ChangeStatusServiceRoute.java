package com.reskitow.thesaferoad.callbacks;

/**
 * Definición de la interfaz que será invocada cuando el estado del servicio cambie.
 */
public interface ChangeStatusServiceRoute {

    /**
     * Llamado cuando el estado del servicio ha sido cambiado.
     *
     * @param isRunning true si el servicio está corriendo, false en caso contrario.
     */
    void onChangeStatusService(boolean isRunning);

}
